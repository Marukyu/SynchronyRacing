local currentLevel = require "necro.game.level.CurrentLevel"
local fastForward = require "necro.client.FastForward"
local playerList = require "necro.client.PlayerList"
local snapshotRequest = require "necro.client.SnapshotRequest"
local tick = require "necro.cycles.Tick"

local function isAloneInRoom()
	return not currentLevel.isLobby() and #playerList.getPlayerList() == 1
end

broadcastSnapshotIfAloneInRoom = tick.delay(function ()
	if isAloneInRoom() then
		snapshotRequest.broadcast()
	end
end)

event.gameStateLevel.add("sendRacingSnapshot", "lobbyLevel", function (ev)
	-- Send snapshot if alone in room
	if isAloneInRoom() and not fastForward.isActive() then
		broadcastSnapshotIfAloneInRoom()
	end
end)

