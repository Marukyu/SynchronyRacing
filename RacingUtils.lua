local racingUtils = {}

local floor = math.floor

-- TODO clean this up?
local placementSuffix = {[11] = "th", [12] = "th"}
local placementModuloSuffix = {[1] = "st", [2] = "nd", [3] = "rd"}

function racingUtils.placementToString(placement)
	if type(placement) == "number" then
		return placement .. (placementSuffix[placement % 100] or placementModuloSuffix[placement % 10] or "th")
	else
		return ""
	end
end

function racingUtils.formatTime(seconds)
	local hours = floor(seconds / 3600)
	local minutes = floor(seconds / 60) % 60
	seconds = seconds % 60
	if hours > 0 then
		return string.format("%d:%02d.%05.2f", hours, minutes, seconds)
	else
		return string.format(minutes > 0 and "%02d:%05.2f" or "%d:%05.2f", minutes, seconds)
	end
end

function racingUtils.formatTimeMilliseconds(seconds)
	local hours = floor(seconds / 3600)
	local minutes = floor(seconds / 60) % 60
	seconds = seconds % 60
	if hours > 0 then
		return string.format("%d:%02d.%06.3f", hours, minutes, seconds)
	else
		return string.format(minutes > 0 and "%02d:%06.3f" or "%d:%06.3f", minutes, seconds)
	end
end

function racingUtils.formatTimeShort(seconds)
	local hours = floor(seconds / 3600)
	local minutes = floor(seconds / 60) % 60
	seconds = floor(seconds) % 60
	if hours > 0 then
		return string.format("%d:%02d.%02d", hours, minutes, seconds)
	else
		return string.format("%02d:%02d", minutes, seconds)
	end
end

return racingUtils
