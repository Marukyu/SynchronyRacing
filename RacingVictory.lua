local racingConfig = require "SynchronyRacing.RacingConfig"
local racingFactions = require "SynchronyRacing.RacingFactions"
local racingMenu = require "SynchronyRacing.RacingMenu"
local racingProtocol = require "SynchronyRacing.RacingProtocol"
local racingRooms = require "SynchronyRacing.RacingRooms"
local racingScore = require "SynchronyRacing.RacingScore"
local racingSpectator = require "SynchronyRacing.RacingSpectator"
local racingTimer = require "SynchronyRacing.RacingTimer"
local racingUtils = require "SynchronyRacing.RacingUtils"

local event = require "necro.event.Event"
local menu = require "necro.menu.Menu"
local playerList = require "necro.client.PlayerList"
local runSummary = require "necro.client.RunSummary"
local speedrunTimer = require "necro.client.SpeedrunTimer"

local color = require "system.utils.Color"
local utils = require "system.utils.Utilities"

local floor = math.floor

victoryAchieved = false

local function formatTime(t)
	if t then
		local minutes = floor(t / 60)
		local seconds = floor(t) % 60
		local milliseconds = floor(t * 1000) % 1000
		return minutes < 1
			and string.format("%d:%02d.%03d", minutes, seconds, milliseconds)
			or string.format("%02d:%02d.%03d", minutes, seconds, milliseconds)
	else
		return "--"
	end
end

local function getVictoryIGT()
	local summary = runSummary.getFinalSummary()
	if summary and summary.victory then
		return summary.duration
	end
end

local function getTeamVictoryTime(teamID)
	local victoryTimes = racingProtocol.clientGetRoomAttribute(racingProtocol.RoomAttribute.VICTORY_TIMES)
	if victoryTimes and victoryTimes[teamID] then
		return victoryTimes[teamID]
	end

	return nil
end

local function getVictoryRTA()
	local victoryTime = getTeamVictoryTime(racingRooms.getClientPlayerTeamID())
	if victoryTime then
		return victoryTime
	end

	local currentLevelTime = speedrunTimer.getCurrentLevelCompletionTime()
	return (racingTimer.getLevelStartRTATime() or speedrunTimer.getCumulativeLevelTime()) + currentLevelTime
end

event.levelComplete.add("racingVictory", {sequence = -1, order = "winScreen"}, function (ev)
	if ev.targetLevel then
		log.info("Level %s reached", ev.targetLevel)
		racingScore.clientSendFlagPlant(ev.targetLevel)
	end
end)

event.runComplete.add("racingVictory", "statistics", function (ev)
	if not victoryAchieved and ev.summary and ev.summary.victory then
		victoryAchieved = true

		-- Notify the server of our victory
		racingScore.clientSendWin()

		-- Compute in-game time and real-time
		log.info("Run complete! RTA: %s, IGT: %s", formatTime(getVictoryRTA()), formatTime(ev.summary.duration))
	end
end)

event.gameStateReset.add("resetVictoryFlag", "init", function (ev)
	victoryAchieved = false
end)

local scoreboardColorSelf = color.hsv(0.18, 0.7, 1)
local scoreboardColorTimeSelf = color.hsv(0.18, 0.3, 1)
local scoreboardColorOther = color.hsv(0, 0, 0.8)

local function listTeams()
	local ownPlacement
	local teams = {}
	for _, playerID in ipairs(playerList.getCrossRoomPlayerList()) do
		local teamID = racingRooms.getClientPlayerTeamID(playerID)
		if teamID and not racingSpectator.isClientSpectator(playerID) then
			teams[teamID] = teams[teamID] or {
				time = tonumber(getTeamVictoryTime(teamID)),
				names = {},
				id = teamID,
				selectableIf = false,
				action = function () end,
			}
			table.insert(teams[teamID].names, playerList.getName(playerID))
		end
	end
	local teamList = utils.getValueList(teams)
	table.sort(teamList, function (a, b)
		if a.time ~= b.time then
			return (a.time or math.huge) < (b.time or math.huge)
		else
			return a.id < b.id
		end
	end)
	local result = {}
	for index, team in ipairs(teamList) do
		local name = "Team " .. team.id
		if #team.names < 3 then
			name = table.concat(team.names, ", ")
		end
		local isSelf = racingRooms.getClientPlayerTeamID() == team.id
		if isSelf and team.time then
			ownPlacement = index
		end
		local col = isSelf and scoreboardColorTimeSelf or scoreboardColorOther
		result[#result + 1] = {
			label = string.format("%s: %s", name, formatTime(team.time)),
			color = color.fade(col, team.time and 1 or 0.75),
		}
	end
	return result, ownPlacement
end

local function getPlacementString(placement)
	return placement and racingUtils.placementToString(placement) .. " place!"
end

local function getScoreboard()
	local scoreboard = {}

	local vicMode = racingConfig.getClientValue(racingConfig.Setting.VICTORY_MODE)
	if vicMode == racingConfig.VictoryMode.TIME then
		local teams, ownPlacement = listTeams()
		utils.appendToTable(scoreboard, {
			{
				label = getPlacementString(ownPlacement) or "Run complete!",
				action = function () end,
			},
			{ height = 0 },
		})
		utils.appendToTable(scoreboard, teams)
		utils.appendToTable(scoreboard, {
			{height = 0},
			{
				label = function ()
					return string.format("In-game time: %s", formatTime(getVictoryIGT()))
				end,
			},
		})
	elseif vicMode == racingConfig.VictoryMode.SCORE then
		local factions = {}
		for _, playerID in ipairs(playerList.getPlayerList()) do
			local factionID = racingFactions.getPlayerFaction(playerID)
			if factionID then
				factions[factionID] = factions[factionID] or {
					score = tonumber(racingFactions.getScore(factionID)) or 0,
					names = {},
					id = factionID,
					selectableIf = false,
					action = function () end,
				}
				table.insert(factions[factionID].names, playerList.getName(playerID))
			end
		end
		local factionList = utils.getValueList(factions)
		table.sort(factionList, function (a, b)
			if a.score ~= b.score then
				return a.score > b.score
			elseif a.id < b.id then
				return a.id < b.id
			end
		end)
		for _, faction in ipairs(factionList) do
			local name = "Team " .. faction.id
			if #faction.names < 3 then
				name = table.concat(faction.names, ", ")
			end
			scoreboard[#scoreboard + 1] = {
				label = string.format("%s: %s points", name, faction.score),
				color = racingFactions.getPlayerFaction(playerList.getLocalPlayerID()) == faction.id
					and scoreboardColorSelf or scoreboardColorOther,
			}
		end
	end

	return scoreboard
end

event.menu.add("racingVictoryRunSummary", {key = "runSummary", sequence = 2}, function (ev)
	if not victoryAchieved then
		return
	end

	local scoreboard = getScoreboard()
	if #scoreboard > 0 then
		scoreboard[1].action = nil
		scoreboard[1].replaceIf = function (entry)
			return entry.id == "duration"
		end
		for _, entry in ipairs(scoreboard) do
			entry.alignX = 0
			entry.x = -300
		end
		racingMenu.replaceEntry(ev, unpack(scoreboard))
		racingMenu.replaceEntry(ev, {
			id = "spectate",
			enableIf = true,
		})
		racingMenu.removeEntries(ev, "summary", "restart", "score")
	end
end)

event.menu.add("racingVictoryPause", {key = "pause", sequence = 2}, function (ev)
	if not victoryAchieved then
		return
	end

	ev.menu.label = "Run complete!"

	local otherEntries = ev.menu.entries
	if ev.arg and ev.arg.hideOtherEntries then
		ev.menu.directionalConfirmation = false
		otherEntries = {{
			label = "Close menu",
			id = "racingVictoryCloseMenu",
			action = function ()
				menu.close()
			end,
		}}
	end

	local scoreboard = getScoreboard()
	if #scoreboard > 0 then
		scoreboard[#scoreboard + 1] = {height = 0}
		ev.menu.entries = utils.copyConcatArrays(scoreboard, otherEntries)
	end

	racingMenu.replaceEntry(ev, {id = "continue", selected = true, label = "Close menu"})

	-- TODO clean up "Mods" menu entry removal (add id to modGrid menu entry)
	racingMenu.removeEntries(ev, "lobby", "restart", function (entry)
		return type(entry.label) == "string" and entry.label:match("Mods")
	end)
end)

event.clientRoomAttribute.add("racingVictoryTimes", racingProtocol.RoomAttribute.VICTORY_TIMES, function (ev)
	menu.update()
end)
