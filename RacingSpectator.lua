local racingSpectator = {}

local racingConfig = require "SynchronyRacing.RacingConfig"
local racingHologram = require "SynchronyRacing.RacingHologram"
local racingProtocol = require "SynchronyRacing.RacingProtocol"
local racingRooms = require "SynchronyRacing.RacingRooms"
local racingUsers = require "SynchronyRacing.RacingUsers"
local menu = require "necro.menu.Menu"
local multiInstance = require "necro.client.MultiInstance"
local netplay = require "necro.network.Netplay"
local playerList = require "necro.client.PlayerList"
local serverClock = require "necro.server.ServerClock"

local serverPlayerList = require "necro.server.ServerPlayerList"
local serverRooms = require "necro.server.ServerRooms"

local event = require "necro.event.Event"
local fastForward = require "necro.client.FastForward"
local focus = require "necro.game.character.Focus"
local gameClient = require "necro.client.GameClient"
local settings = require "necro.config.Settings"
local spectator = require "necro.game.character.Spectator"
local tick = require "necro.cycles.Tick"
local utils = require "system.utils.Utilities"

spectatorHUDLimit = settings.shared.number {
	name = "Hide spectator HUD",
	minimum = 1,
	default = 5,
	format = function (value)
		return value <= 1 and "Always" or string.format("%s+ players", value)
	end,
	order = 71,
}

function racingSpectator.isClientSpectator(playerID)
	return racingProtocol.clientGetPlayerAttribute(playerID, racingProtocol.PlayerAttribute.SPECTATOR)
end

function racingSpectator.isClientBroadcaster(playerID)
	return not not racingProtocol.clientGetPlayerAttribute(playerID, racingProtocol.PlayerAttribute.BROADCASTER)
end

local function promoteSpectatorToHost(playerID)
	local roomID = serverPlayerList.getRoomID(playerID)
	if racingUsers.isAdmin(racingUsers.lookUpUserByPlayerID(playerID)) and serverRooms.isValidRoom(roomID) then
		serverRooms.setHost(roomID, playerID)
	end
end

--- @param match SynchronyRacingMatch
--- @param playerID integer
local function tryJoinMatchAsSpectator(match, playerID)
	local userID = racingUsers.lookUpUserByPlayerID(playerID)

	-- Skip users that are not connected or already in a match
	if racingUsers.isInMatch(userID) or not serverPlayerList.isLoggedIn(playerID) then
		return
	end

	-- Check which slot (team index) the user should spectate
	local spectatorSlot = racingUsers.getSpectatorSlot(userID)

	-- Enable auto-spectate by setting
	if type(spectatorSlot) ~= "number" and racingConfig.getValueForMatch(match, racingConfig.Setting.AUTO_SPECTATE) then
		spectatorSlot = 1
	end

	if type(spectatorSlot) == "number" and #match.teams ~= 0 then
		-- Compute effective spectator slot by wrapping around greater indices
		spectatorSlot = (spectatorSlot - 1) % #match.teams + 1

		-- Join the team's associated race room
		local team = match.teams[spectatorSlot]
		if team and serverRooms.isValidRoom(team.room) then
			serverRooms.setPlayerRoom(playerID, team.room)
			racingProtocol.serverSetPlayerAttribute(playerID, racingProtocol.PlayerAttribute.SPECTATOR, true)
			racingProtocol.serverSetPlayerAttribute(playerID, racingProtocol.PlayerAttribute.PARTICIPANT, false)

			-- TODO check if this works
			racingProtocol.serverSetPlayerAttribute(playerID, racingProtocol.PlayerAttribute.TEAM, spectatorSlot)
			racingProtocol.serverSetPlayerAttribute(playerID, racingProtocol.PlayerAttribute.MATCH, match.id)

			promoteSpectatorToHost(playerID)
		end
	end
end

local function roomContainsSpectatorClient(roomID, parentID)
	if serverRooms.isValidRoom(roomID) then
		for _, playerID in ipairs(serverRooms.playersInRoom(roomID)) do
			if serverPlayerList.getAttribute(playerID, racingProtocol.PlayerAttribute.PARENT) == parentID
				and serverPlayerList.isConnected(playerID)
			then
				return true
			end
		end
	end
	return false
end

--- @param match SynchronyRacingMatch
--- @param playerID integer
function racingSpectator.initAutoSpectateOpponent(match, playerID, disallowMultiple, targetTeamID)
	local parentID = serverPlayerList.getAttribute(playerID, racingProtocol.PlayerAttribute.PARENT)
	if parentID and match then
		if targetTeamID == nil and racingRooms.isRacingRoom(serverPlayerList.getRoomID(playerID)) then
			return nil
		end

		local roomID = serverPlayerList.getRoomID(parentID)
		for teamID, team in ipairs(match.teams) do
			if (team.room ~= roomID or #match.teams == 1) and serverRooms.isValidRoom(team.room)
				and serverPlayerList.getAttribute(playerID, racingProtocol.PlayerAttribute.SPECTATOR) ~= false
				and (not disallowMultiple or not roomContainsSpectatorClient(team.room, parentID))
				and (not targetTeamID or teamID >= targetTeamID)
			then
				serverPlayerList.setAttribute(playerID, racingProtocol.PlayerAttribute.TEAM, teamID)
				serverPlayerList.setAttribute(playerID, racingProtocol.PlayerAttribute.MATCH, match.id)
				serverPlayerList.setAttribute(playerID, racingProtocol.PlayerAttribute.SPECTATOR, true)
				serverPlayerList.setAttribute(playerID, racingProtocol.PlayerAttribute.PARTICIPANT, false)
				return team.room
			end
		end
	end

	if targetTeamID then
		return racingSpectator.initAutoSpectateOpponent(match, playerID, disallowMultiple, false)
	end

	if disallowMultiple then
		return racingSpectator.initAutoSpectateOpponent(match, playerID)
	end
end

local function isParentBroadcaster(playerID)
	return racingProtocol.serverGetPlayerAttribute(racingProtocol.serverGetPlayerAttribute(playerID,
		racingProtocol.PlayerAttribute.PARENT), racingProtocol.PlayerAttribute.BROADCASTER)
end

--- @param match SynchronyRacingMatch
function racingSpectator.handleAutoSpectate(match, players)
	for _, playerID in ipairs(players or serverPlayerList.allPlayers()) do
		local roomID = racingSpectator.initAutoSpectateOpponent(match, playerID, isParentBroadcaster(playerID), nil)
		if roomID then
			-- Auto-spectate opponent
			serverRooms.setPlayerRoom(playerID, roomID)
			promoteSpectatorToHost(playerID)
		else
			-- Auto-spectate by slot
			tryJoinMatchAsSpectator(match, playerID)
		end
	end
end

function racingSpectator.cycleTarget(playerID)
	if serverPlayerList.isLoggedIn(playerID)
		and serverPlayerList.getAttribute(playerID, racingProtocol.PlayerAttribute.SPECTATOR)
	then
		local match = racingRooms.getMatchForRoomID(serverPlayerList.getRoomID(playerID))
		local teamID = serverPlayerList.getAttribute(playerID, racingProtocol.PlayerAttribute.TEAM)
		if type(teamID) == "number" and match then
			local roomID = racingSpectator.initAutoSpectateOpponent(match, playerID, false, teamID + 1)
			if roomID then
				serverRooms.setPlayerRoom(playerID, roomID)
			end
		end
	end
end

function racingSpectator.requestSpectate(playerID, spectate)
	racingProtocol.serverSendMessage(playerID, racingProtocol.MessageType.SPECTATE, spectate)
end

local requestSpectate = tick.registerDelay(function (args)
	if gameClient.isConnected() then
		if fastForward.isActive() then
			-- Retry
			return false
		end
		spectator.setSpectating(nil, args == true)
	end
end)

event.clientMessage.add("requestSpectate", racingProtocol.MessageType.SPECTATE, function (message)
	requestSpectate(message)
end)

event.serverEnterRoom.add("resetSpectatorAttributes", {order = "playerList", sequence = 1}, function (ev)
	if ev.roomID == serverRooms.getDefaultRoomID() then
		racingProtocol.serverSetPlayerAttribute(ev.playerID, racingProtocol.PlayerAttribute.SPECTATOR, nil)
		racingProtocol.serverSetPlayerAttribute(ev.playerID, racingProtocol.PlayerAttribute.TEAM, nil)
	end
end)

local attribSpectator = racingProtocol.PlayerAttribute.SPECTATOR
local attribParent = racingProtocol.PlayerAttribute.PARENT

local function isSpectator(playerID)
	return playerList.getAttribute(playerID, attribSpectator)
end

local function isParented(playerID)
	return playerList.getAttribute(playerID, attribParent)
end

event.serverMessage.add("requestSpectatorCycle", racingProtocol.MessageType.SPECTATE_CYCLE, function (ev)
	if ev.message and ev.message.confirm then
		serverPlayerList.setAttribute(ev.playerID, racingProtocol.PlayerAttribute.SPECTATE_CYCLE_LOCKOUT, nil)
		return
	end

	for _, playerID in ipairs(serverPlayerList.allPlayers()) do
		if playerID ~= ev.playerID
			and serverPlayerList.getAttribute(playerID, attribParent) == ev.playerID
			and serverPlayerList.getAttribute(playerID, attribSpectator)
		then
			local time = serverClock.getTime()
			local lockout = serverPlayerList.getAttribute(playerID, racingProtocol.PlayerAttribute.SPECTATE_CYCLE_LOCKOUT)
			if not lockout or time > lockout then
				serverPlayerList.setAttribute(playerID, racingProtocol.PlayerAttribute.SPECTATE_CYCLE_LOCKOUT, time + 5)
				racingSpectator.cycleTarget(playerID)
			end
		end
	end
end)

event.fastForwardComplete.add("confirmSpecatatorCycle", "menu", function (ev)
	if gameClient.isConnected() and isSpectator() then
		gameClient.sendReliable(racingProtocol.MessageType.SPECTATE_CYCLE, { confirm = true })
	end
end)

event.renderPlayerList.add("hideSpectators", {order = "filter"}, function (ev)
	if racingProtocol.clientGetRoomAttribute(racingProtocol.RoomAttribute.ROOM_TYPE) ~= racingRooms.RoomType.RACE then
		utils.removeIf(ev.list, isParented)
	elseif not menu.isOpen() then
		utils.removeIf(ev.list, isSpectator)
	end
end)

event.updateFocusedEntities.add("addSpectatorTargets", {order = "spectatorTargets", sequence = 1}, function (ev)
	if ev.entities and spectator.isActive() then
		if multiInstance.isDuplicate() and not racingHologram.getElementVisibility("showPlayerHearts") then
			for _, entity in ipairs(ev.entities) do
				if entity.focusable then
					entity.focusable.flags = focus.Flag.mask(entity.focusable.flags, focus.Flag.HIDE_HEALTH_BAR)
				end
			end
		end
		if #ev.entities >= spectatorHUDLimit then
			for _, entity in ipairs(ev.entities) do
				if entity.focusable then
					entity.focusable.flags = focus.Flag.unmask(entity.focusable.flags, focus.Flag.HUD)
				end
			end
		end
	end
end)

event.serverConnect.add("unoccupyPlayerSlot", {order = "accept", sequence = 1}, function (ev)
	-- Temporarily un-occupy slot while the player is connecting
	serverPlayerList.setAttribute(ev.playerID, netplay.PlayerAttribute.OCCUPY_SLOT, false)
end)

local function handleLogin(ev)
	if serverPlayerList.isConnected(ev.message.parent) then
		-- Reject mismatching secret
		if serverPlayerList.getAttribute(ev.message.parent, netplay.PlayerAttribute.SESSION_SECRET) ~= ev.message.secret then
			ev.rejection = L("Mismatching client secret", "error.mismatchingClientSecret")
			log.warn("Client secret mismatch for player %s", ev.playerID)
			return
		end

		-- Kick previous non-admin spectator client
		for _, playerID in ipairs(serverPlayerList.allPlayers()) do
			if playerID ~= ev.playerID and serverPlayerList.getAttribute(playerID, racingProtocol.PlayerAttribute.PARENT) == ev.message.parent then
				local userID = racingUsers.lookUpUserByPlayerID(ev.message.parent)
				if not racingUsers.isAdmin(userID) and not racingUsers.getSpectatorSlot(userID) then
					serverPlayerList.disconnectPlayer(playerID)
				end
			end
		end

		-- Don't count towards lobby capacity limit
		return true
	elseif ev.message.parent then
		-- Reject non-zero non-connected parent
		ev.rejection = L("Primary player not connected", "error.primaryPlayerNotConnected")
		log.warn("Primary player %s not connected for sub-player %s", ev.message.parent, ev.playerID)
	end
end

event.serverCheckLogIn.add("allowLobbyLimitBypass", {order = "playerCount", sequence = -1}, function (ev)
	local occupySlot = nil
	if handleLogin(ev) then
		occupySlot = false
	end
	serverPlayerList.setAttribute(ev.playerID, netplay.PlayerAttribute.OCCUPY_SLOT, occupySlot)
end)

event.serverLogIn.add("silenceJoinMessage", {order = "chat", sequence = -1}, function (ev)
	if serverPlayerList.getAttribute(ev.playerID, racingProtocol.PlayerAttribute.PARENT) then
		ev.silent = true
	end
end)

event.serverDisconnect.add("silenceLeaveMessage", {order = "chat", sequence = -1}, function (ev)
	if serverPlayerList.getAttribute(ev.playerID, racingProtocol.PlayerAttribute.PARENT) then
		ev.silent = true
	end
end)

return racingSpectator
