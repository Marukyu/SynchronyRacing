local racingConfig = require "SynchronyRacing.RacingConfig"
local racingMatch = require "SynchronyRacing.RacingMatch"
local racingProtocol = require "SynchronyRacing.RacingProtocol"
local racingRooms = require "SynchronyRacing.RacingRooms"
local racingTimer = require "SynchronyRacing.RacingTimer"
local ability = require "necro.game.system.Ability"
local affectorItem = require "necro.game.item.AffectorItem"
local cameraTarget = require "necro.render.CameraTarget"
local components = require "necro.game.data.Components"
local currentLevel = require "necro.game.level.CurrentLevel"
local customActions = require "necro.game.data.CustomActions"
local customEntities = require "necro.game.data.CustomEntities"
local delay = require "necro.game.system.Delay"
local ensemble = require "necro.game.data.modifier.Ensemble"
local flyaway = require "necro.game.system.Flyaway"
local freeze = require "necro.game.character.Freeze"
local gameSession = require "necro.client.GameSession"
local gameState = require "necro.client.GameState"
local interaction = require "necro.game.object.Interaction"
local inventory = require "necro.game.item.Inventory"
local move = require "necro.game.system.Move"
local multiCharacter = require "necro.game.data.modifier.MultiCharacter"
local netplay = require "necro.network.Netplay"
local object = require "necro.game.object.Object"
local objectMap = require "necro.game.object.Map"
local player = require "necro.game.character.Player"
local playerList = require "necro.client.PlayerList"
local render = require "necro.render.Render"
local rng = require "necro.game.system.RNG"
local screenShake = require "necro.render.ScreenShake"
local serverClock = require "necro.server.ServerClock"
local serverPlayerList = require "necro.server.ServerPlayerList"
local settings = require "necro.config.Settings"
local settingsStorage = require "necro.config.SettingsStorage"
local snapshot = require "necro.game.system.Snapshot"
local sound = require "necro.audio.Sound"
local swipe = require "necro.game.system.Swipe"
local tick = require "necro.cycles.Tick"
local tile = require "necro.game.tile.Tile"
local tileTypes = require "necro.game.tile.TileTypes"
local trapClientTrigger = require "necro.game.trap.TrapClientTrigger"
local ui = require "necro.render.UI"

local ecs = require "system.game.Entities"
local gfx = require "system.gfx.GFX"
local utils = require "system.utils.Utilities"

local field = components.field
local constant = components.constant
local dependency = components.dependency

local sessionKey = "gameplay.modifiers.ensemble.choiceOrder.session"

ensembleLockedPlayers = snapshot.levelVariable()

lastHandledStartID = nil
disintegrationIncidentOccurred = nil

components.register {
	SynchronyRacing_itemSuppressAllActions = {},
}

local actionInhibitor = customEntities.register {
	name = "SynchronyRacing_ItemActionInhibitor",
	gameObject = {},
	position = {},
	item = {},
	SynchronyRacing_itemSuppressAllActions = {},
}

--- @class SynchronyRacing.Ensemble
--- @field initialized boolean
--- @field startTime number

local function getPendingOrder()
	local order = {}
	for i = 1, #multiCharacter.getCharacterList() do
		order[i] = false
	end
	for entity in ecs.entitiesWithComponents {"interactableSelectEnsemble"} do
		local index = entity.interactableSelectEnsemble.index
		if order[index] ~= nil then
			order[index] = entity.interactableSelectCharacter.characterType
		end
	end
	return order
end

local function arrayEquals(t1, t2)
	return type(t1) == "table" and type(t2) == "table" and utils.arrayEquals(t1, t2)
end

local function validateEnsembleChoiceList(list1, list2)
	if type(list1) == "table" and type(list2) == "table" and #list1 == #list2 then
		local map = {}
		for i = 1, #list1 do
			local value = list1[i]
			if not value then
				return false
			elseif map[value] == nil then
				map[value] = 1
			else
				map[value] = map[value] + 1
			end
		end
		for i = 1, #list2 do
			local value = list2[i]
			if map[value] == 1 then
				map[value] = nil
			elseif map[value] then
				map[value] = map[value] - 1
			else
				return false
			end
		end
		return next(map) == nil
	end
	return false
end

local function randomizeRemainingChoices(modeList, choiceList)
	if type(modeList) ~= "table" then
		modeList = {}
	end
	if type(choiceList) ~= "table" then
		choiceList = {}
	else
		choiceList = utils.arrayCopy(choiceList)
	end

	local map = {}
	for i = 1, #modeList do
		local value = modeList[i] or false
		if map[value] == nil then
			map[value] = 1
		else
			map[value] = map[value] + 1
		end
	end

	local badIndices = {}
	for i = 1, #modeList do
		local value = choiceList[i]
		if map[value] == 1 then
			map[value] = nil
		elseif map[value] then
			map[value] = map[value] - 1
		else
			choiceList[i] = false
			badIndices[#badIndices + 1] = i
		end
	end

	if badIndices[1] then
		rng.shuffle(badIndices, rng.makeChannel(math.random(1, 10000), math.random(1, 10000), math.random(1, 10000)))
		local badCount = 1
		for charType, count in pairs(map) do
			for i = 1, count do
				local badIndex = badIndices[badCount]
				if badIndex then
					choiceList[badIndex] = charType
				else
					log.warn("Ensemble choice index mismatch")
					break
				end
				badCount = badCount + 1
			end
		end
	end

	return choiceList
end

local function isEnsembleChoiceRoom()
	return ensemble.isActive() and currentLevel.isSafe() and not currentLevel.isLobby()
end

event.levelLoad.add("ensembleOverride", {order = "lobbyLevel", sequence = 1}, function (ev)
	if isEnsembleChoiceRoom() then
		for entity in ecs.entitiesWithComponents {"trapEnsembleModeStart"} do
			local label = objectMap.firstWithComponent(entity.position.x, entity.position.y, "worldLabel")
			if label then
				label.worldLabel.text = "Ready"
				if label.worldLabelTextPool then
					label.worldLabelTextPool.key = ""
				end
			end
		end

		local zoneID = tileTypes.lookUpZoneID("Boss")
		if zoneID then
			local substitutions = {
				UnbreakableWall = "BossWallNecrodancer",
			}
			local levelX, levelY, levelW, levelH = tile.getLevelBounds()
			for y = levelY, levelY + levelH - 1 do
				for x = levelX, levelX + levelW - 1 do
					local name = tile.getInfo(x, y).name
					local tileID = tileTypes.lookUpTileID(substitutions[name] or name, zoneID)
					if tileID then
						tile.set(x, y, tileID)
					end
				end
			end
		end
	end
end)

local function playersStandingOnReadyTile()
	local hasValidPlayer = false
	for _, entity in ipairs(player.getPlayerEntities()) do
		if entity.gameObject.tangible then
			hasValidPlayer = true
			if not objectMap.hasComponent(entity.position.x, entity.position.y, "trapEnsembleModeStart") then
				return false
			end
		end
	end
	return hasValidPlayer
end


event.trapTrigger.add("ensembleReady", {order = "spell", filter = "trapEnsembleModeStart"}, function (ev)
	flyaway.createIfFocused {
		entity = ev.victim,
		text = "Ready!",
	}

	if ev.victim.character then
		ensembleLockedPlayers = ensembleLockedPlayers or {}
		ensembleLockedPlayers[ev.victim.id] = true
		ev.victim.character.canAct = false
		if ev.victim.inventory and not affectorItem.entityHasItem(ev.victim, "SynchronyRacing_itemSuppressAllActions") then
			object.delayedDelete(inventory.grant(actionInhibitor, ev.victim), 1)
		end
	end
end)

event.holderCheckAbility.add("ensembleLock", {order = "stun", filter = "SynchronyRacing_itemSuppressAllActions"}, function (ev)
	if ability.Flag.check(ev.flags, ability.Flag.CHECK_STUN) then
		ev.flags = ability.Flag.mask(ev.flags, ability.Flag.IGNORE_ACTION)
	end
end)

local function getUnselectedEnsembleShrines()
	local unselected = {}
	for entity in ecs.entitiesWithComponents {"interactableSelectEnsemble"} do
		if entity.interactableSelectEnsemble.index == 0 then
			unselected[#unselected + 1] = entity
		end
	end
	return unselected
end

fixIncompleteChoiceOrder = delay.new(function (entity)
	if isEnsembleChoiceRoom() then
		local unselected = getUnselectedEnsembleShrines()
		if unselected[1] then
			screenShake.createAtPosition(0, 0, 0.25, 16, 100)
			for _, choice in ipairs(ensemble.getSessionOrder()) do
				if not unselected[1] then
					break
				end
				for i, shrine in ipairs(unselected) do
					if shrine.interactableSelectCharacter.characterType == choice then
						interaction.perform(entity, shrine)
						table.remove(unselected, i)
						break
					end
				end
			end
		end
	end
end)

event.swipe.add("disintegration", "SynchronyRacing_disintegration", function (ev)
	swipe.inherit(ev, "explosion")
	ev.swipe.texture = "mods/SynchronyRacing/gfx/disintegration.png"
end)

disintegratePlayer = delay.new(function (entity)
	sound.playFile("mods/SynchronyRacing/sfx/disintegration.ogg")
	swipe.create {
		type = "SynchronyRacing_disintegration",
		x = entity.position and entity.position.x or 0,
		y = entity.position and entity.position.y or 0,
	}
	if entity.sprite then
		entity.sprite.mirrorOffsetX = entity.sprite.mirrorOffsetX / 4
		entity.sprite.mirrorX = entity.sprite.mirrorX * 4
		entity.sprite.visible = false
	end
	if entity.rowOrder then
		entity.rowOrder.z = -100
	end
	freeze.inflict(entity, nil, freeze.Type.INVISIBLE)
	screenShake.createOnEntity(entity, 1, 20)
end)

moveDeferred = delay.new(function (entity, args)
	move.absolute(entity, args.x, args.y, move.Flag.TWEEN)
	if entity.tween then
		entity.tween.maxHeight = 50
	end
	local playerEntity = ecs.getEntityByID(args.p)
	if playerEntity and playerEntity.sprite then
		playerEntity.sprite.visible = true
	end
end)

event.entitySchemaLoadNamedEntity.add("CharacterSelectorEnsembleRandom", "CharacterSelectorEnsembleRandom", function (ev)
	ev.entity.tween = { duration = 0.5 }
end)

event.holderUpdateScale.add("disintegrationScale", {order = "sprite", sequence = -1, filter = "SynchronyRacing_itemSuppressAllActions"}, function (ev)
	if disintegrationIncidentOccurred and ev.holder.sprite and math.abs(ev.holder.sprite.mirrorX) >= 2 then
		ev.scale = ev.scale * 0.25
	end
end)

local _, finalizeEnsembleChoiceRoom = customActions.registerSystemAction {
	id = "FinalizeEnsembleChoiceRoom",
	callback = function ()
		if isEnsembleChoiceRoom() then
			for entity in ecs.entitiesWithComponents {"worldLabelTextPool"} do
				if entity.worldLabelTextPool.key == "label.lobby.ensemble.choose" then
					object.delete(entity)
				end
			end
			for _, entity in ipairs(player.getPlayerEntities()) do
				if entity.inventory then
					inventory.grant(actionInhibitor, entity)
				end
			end
			if not validateEnsembleChoiceList(getPendingOrder(), multiCharacter.getCharacterList()) then
				settingsStorage.set("video.screenShake", 1, settings.Layer.SCRIPT_OVERRIDE)
				disintegrationIncidentOccurred = true
				local randoShrine = object.firstWithComponents {"interactableSelectEnsembleRandom"}
				for _, entity in ipairs(player.getPlayerEntities()) do
					if entity.gameObject.tangible then
						disintegratePlayer(entity, nil, 0.5)
						if randoShrine then
							moveDeferred(randoShrine, {
								x = randoShrine.position.x,
								y = randoShrine.position.y,
								p = entity.id,
							}, 2)
							move.absolute(randoShrine, entity.position.x, entity.position.y, move.Flag.TWEEN)
							if randoShrine.tween then
								randoShrine.tween.maxHeight = 100
							end
							randoShrine = nil
						end
					end
				end
				local primary = player.getPlayerEntity(trapClientTrigger.getPrimaryPlayer(false))
				if primary then
					fixIncompleteChoiceOrder(primary, nil, 2.5)
				end
			end
		end
	end,
}

event.turn.add("ensembleOrder", {order = "lobbyLevel", sequence = 1}, function (ev)
	if isEnsembleChoiceRoom() then
		if ensembleLockedPlayers then
			for _, entity in ipairs(player.getPlayerEntities()) do
				if ensembleLockedPlayers[entity.id]
					and not objectMap.hasComponent(entity.position.x, entity.position.y, "trapEnsembleModeStart")
				then
					ensembleLockedPlayers[entity.id] = nil
					flyaway.createIfFocused {
						entity = entity,
						text = "Not ready",
					}
				end
			end
		end

		if playerList.isHost() then
			local order = getPendingOrder()
			local attrib = racingProtocol.RoomAttribute.ENSEMBLE_ORDER
			local currentValue = racingProtocol.clientGetRoomAttribute(attrib)
			if not arrayEquals(order, currentValue) then
				racingProtocol.clientSetRoomAttribute(attrib, order)
			end
			racingProtocol.clientSetRoomAttribute(racingProtocol.RoomAttribute.ENSEMBLE_READY, playersStandingOnReadyTile())
		end
	end
end)

restartAfterDelay = tick.delay(function ()
	if isEnsembleChoiceRoom() then
		gameSession.restart()
	end
end)

event.tick.add("ensembleAutoStart", {order = "lobbyLevel", sequence = 1}, function (ev)
	if isEnsembleChoiceRoom() and playerList.isHost() then
		local startID = racingProtocol.clientGetRoomAttribute(racingProtocol.RoomAttribute.ENSEMBLE_START)
		if startID and lastHandledStartID ~= startID then
			lastHandledStartID = startID
			finalizeEnsembleChoiceRoom()
			if validateEnsembleChoiceList(getPendingOrder(), multiCharacter.getCharacterList()) then
				gameSession.restart()
			else
				restartAfterDelay(nil, 3)
			end
		end
	end
end)

event.clientChangeRoom.add("resetRacingEnsembleStart", {order = "resources", sequence = -1}, function (ev)
	lastHandledStartID = nil
	disintegrationIncidentOccurred = nil
end)

event.clientDisconnect.add("resetRacingEnsembleStart", {order = "reset", sequence = 1}, function (ev)
	lastHandledStartID = nil
	disintegrationIncidentOccurred = nil
end)

event.SynchronyRacing_roomCreate.add("initEnsembleMode", "ensemble", function (ev)
	local mode = multiCharacter.Mode.data[ev.settings["gameplay.modifiers.multiChar"]]
	if mode.ensemble then
		-- Allow players to go fast in the multichar selection room
		ev.settings["lobby.ignoreRhythm"] = true

		-- Always lock camera
		ev.settings["camera.lockToBoss"] = cameraTarget.BossLockMode.ALWAYS

		-- Reset choice order (if any was set)
		ev.settings[sessionKey] = nil

		-- Skip usual char select countdown
		ev.settings[racingConfig.Setting.START_COUNTDOWN] = 0

		-- Store multichar mode type in room
		racingProtocol.serverSetRoomAttribute(ev.roomID, racingProtocol.RoomAttribute.ENSEMBLE,
			ev.settings["gameplay.modifiers.multiChar"])

		-- Initialize Ensemble mode struct
		ev.match.ensemble = {
			id = ev.settings["gameplay.modifiers.multiChar"],
		}
	elseif ev.match.ensemble then
		-- Remove Ensemble mode struct if it shouldn't be there
		ev.match.ensemble = nil
	end
end)

local function isNonEmptyList(tbl)
	return type(tbl) == "table" and tbl[1] ~= nil
end

event.serverMessage.add("pingEnsembleCooldownOverride", {key = "PING_TOOL", sequence = 1}, function (ev)
	local roomID = serverPlayerList.getRoomID(ev.playerID)
	if racingProtocol.serverGetRoomAttribute(roomID, racingProtocol.RoomAttribute.ENSEMBLE)
		and not isNonEmptyList(racingConfig.getValueForRoom(roomID, sessionKey))
	then
		serverPlayerList.setAttribute(ev.playerID, netplay.PlayerAttribute.PING_COOLDOWN, 0)
	end
end)

local function allTeamsEnsembleReady(match, mode)
	local time = serverClock.getTime() - match.startTimestamp

	if time >= racingConfig.getValueForMatch(match, racingConfig.Setting.ENSEMBLE_PREP_TIME_MAX) then
		return true
	end

	if time >= racingConfig.getValueForMatch(match, racingConfig.Setting.ENSEMBLE_PREP_TIME_MIN) then
		for _, team in ipairs(match.teams) do
			if not racingProtocol.serverGetRoomAttribute(team.room, racingProtocol.RoomAttribute.ENSEMBLE_READY)
				or not validateEnsembleChoiceList(racingProtocol.serverGetRoomAttribute(team.room,
				racingProtocol.RoomAttribute.ENSEMBLE_ORDER), mode.characters)
			then
				return false
			end
		end
		return true
	end

	return false
end

event.SynchronyRacing_matchTick.add("checkEnsemble", "ensemble", function (ev)
	local state = ev.match.ensemble
	if state and ev.match.startTimestamp and ev.match.status == racingMatch.Status.ACTIVE then
		local mode = multiCharacter.Mode.data[state.id]

		if not state.initialized then
			state.initialized = true

			-- Allow all characters
			if type(mode.characters) == "table" then
				for _, team in ipairs(ev.teams) do
					racingConfig.modifyRoomSettings(team.room, {
						["mod.SynchronyRacing.allowedPlayerCharacters"] = utils.listToSet(mode.characters)
					})
				end
			end
		end

		if not state.startTime and allTeamsEnsembleReady(ev.match, mode) then
			state.startTime = serverClock.getTime() + racingConfig.getValueForMatch(ev.match, racingConfig.Setting.ENSEMBLE_START_DELAY)
			for _, team in ipairs(ev.teams) do
				-- Racers too slow to select stuff: force choices
				local roomOrder = randomizeRemainingChoices(mode.characters, racingProtocol.serverGetRoomAttribute(
					team.room, racingProtocol.RoomAttribute.ENSEMBLE_ORDER))
				racingConfig.modifyRoomSettings(team.room, { [sessionKey] = roomOrder })
				racingProtocol.serverSetRoomAttribute(team.room, racingProtocol.RoomAttribute.ENSEMBLE_START, ev.matchID)
				racingTimer.serverSetRTATimestamp(team.room, state.startTime)
			end
		end
	end
end)

event.serverChangeGameState.add("handleEnsembleStart", {order = "timestamp", sequence = -1}, function (ev)
	if ev.playerID and ev.SynchronyRacing_suppressed == false then
		local match = racingRooms.getMatchForRoomID(ev.roomID)
		local startTime = match and match.ensemble and match.ensemble.startTime
		if startTime and serverClock.getTime() < startTime then
			ev.SynchronyRacing_time = startTime
			ev.message.countdown = {text = "Starting in"}
		end
	end
end)

event.renderUI.override("drawCountdown", 1, function (func, ev)
	if isEnsembleChoiceRoom() and racingProtocol.clientGetRoomAttribute(racingProtocol.RoomAttribute.ENSEMBLE_START) then
		local countdown = gameState.getCountdown()
		if countdown then
			if not disintegrationIncidentOccurred or countdown <= 5 then
				ui.drawText {
					buffer = render.Buffer.UI_OVERLAY,
					font = ui.Font.MEDIUM,
					size = ui.Font.MEDIUM.size * ui.getScaleFactor(),
					text = string.format("Starting in: %d", math.ceil(math.max(countdown, 1))),
					x = gfx.getWidth() * 0.5,
					y = gfx.getHeight() * 0.1,
					alignX = 0.5,
				}
			end
			return
		end
	end
	return func(ev)
end)

local function hideDuringEnsembleCharSelect(func, ev)
	if not isEnsembleChoiceRoom() then
		return func(ev)
	end
end

event.renderGlobalHUD.override("renderPlayerHUDs", 1, hideDuringEnsembleCharSelect)
