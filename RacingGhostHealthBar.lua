local health = require "necro.game.character.Health"
local render = require "necro.render.Render"
local visualExtent = require "necro.render.level.VisualExtent"

local ecs = require "system.game.Entities"
local gfx = require "system.gfx.GFX"

local min = math.min
local floor = math.floor
local ceil = math.ceil

local function renderHealthBar(entity, drawImage)
	if entity.sprite and not entity.sprite.visible then
		return false
	end

	local hearts = health.getHearts(entity)
	local columns = entity.healthBar.columns

	local heartCount = #hearts
	if entity.healthBar.alignRight and heartCount > columns then
		local shift = heartCount % columns
		if shift ~= 0 then
			heartCount = heartCount - shift + columns
			for i = heartCount, heartCount - columns + 1, -1 do
				hearts[i] = heartCount - i + 1 <= shift and hearts[i - columns + shift]
			end
		end
	end

	local spacingX = entity.healthBar.spacingX
	local spacingY = entity.healthBar.spacingY
	local x, y = visualExtent.getOrigin(entity) + entity.healthBar.offsetX, entity.shadowPosition.y + entity.healthBar.offsetY

	do
		local primaryHeart = health.Heart.data[hearts[1]].imageWorld
		if primaryHeart then
			local width, height = gfx.getImageSize(primaryHeart)
			spacingX = spacingX + width
			spacingY = spacingY + height
			x = x - ceil(width * 0.5)
		end
	end

	local crop = entity.croppedSprite and entity.croppedSprite.bottom or 0

	x = x - spacingX * (min(heartCount, columns) * 0.5 - 0.5)
	y = y - spacingY * (ceil(heartCount / columns) - 1) + crop
	local heartData = health.Heart.data
	for i = 1, heartCount do
		local heart = heartData[hearts[i]]
		if heart and heart.imageWorld then
			drawImage(heart.imageWorld,
				x + spacingX * ((i - 1) % columns),
				y + spacingY * floor((i - 1) / columns), y, 0.5)
		end
	end
end

event.render.add("renderGhostHealthBars", {order = "healthBars", sequence = -1}, function ()
	local drawImage = render.getBuffer(render.Buffer.HEALTH_BAR).drawImage
	for entity in ecs.entitiesWithComponents {"SynchronyRacing_playerGhost", "visibility", "healthBar"} do
		if entity.visibility.visible then
			renderHealthBar(entity, drawImage)
			entity.healthBar.visible = false
		end
	end
end)
