local racingMatchmaking = {}

local racingMatch = require "SynchronyRacing.RacingMatch"
local racingUsers = require "SynchronyRacing.RacingUsers"
local rng = require "necro.game.system.RNG"

local event = require "necro.event.Event"
local settings = require "necro.config.Settings"

autoMatchSettingsGroup = settings.group {
	id = "autoMatch",
	name = "Matchmaking settings",
	order = 2000,
}

autoMatchEnabled = settings.shared.bool {
	id = "autoMatch.enabled",
	name = "Enable matchmaking",
	default = false,
}

autoMatchTeamCount = settings.shared.number {
	id = "autoMatch.teamCount",
	name = "Matchmaking team count",
	default = 2,
	minimum = 2,
	maximum = 16,
}

autoMatchMinPlayers = settings.shared.number {
	id = "autoMatch.minPlayers",
	name = "Matchmaking minimum team size",
	default = 1,
	minimum = 1,
	maximum = 8,
}

autoMatchMaxPlayers = settings.shared.number {
	id = "autoMatch.maxPlayers",
	name = "Matchmaking maximum team size",
	default = 2,
	minimum = 1,
	maximum = 8,
}

local standaloneRNGChannel = {
	state1 = math.random(0, 2^24-1),
	state2 = math.random(0, 2^24-1),
	state3 = math.random(0, 2^24-1),
}

--- Generates an automatic matchup
--- @return SynchronyRacingMatch|nil
function racingMatchmaking.nextMatch()
	if autoMatchEnabled then
		local users = racingUsers.getAvailableUsers()
		local teamCount = autoMatchTeamCount
		local effectiveUserCount = math.floor(math.min(#users / teamCount, autoMatchMaxPlayers)) * teamCount
		if effectiveUserCount >= autoMatchMinPlayers * teamCount then
			rng.shuffle(users, standaloneRNGChannel)
			local teams = {}
			for i = 1, teamCount do
				teams[i] = {users = {}}
			end
			for i = 1, effectiveUserCount do
				local user = users[i]
				local teamIndex = (i - 1) % teamCount + 1
				table.insert(teams[teamIndex].users, user)
			end
			return {
				status = racingMatch.Status.PENDING,
				teams = teams,
			}
		end
	end
end

function racingMatchmaking.isEnabled()
	return autoMatchEnabled
end

return racingMatchmaking
