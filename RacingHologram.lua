local racingHologram = {}

local racingConfig = require "SynchronyRacing.RacingConfig"
local racingProtocol = require "SynchronyRacing.RacingProtocol"
local racingSpectator = require "SynchronyRacing.RacingSpectator"
local customActions = require "necro.game.data.CustomActions"
local gameClient = require "necro.client.GameClient"
local gameWindow = require "necro.config.GameWindow"
local hud = require "necro.render.hud.HUD"
local menu = require "necro.menu.Menu"

local multiInstance = require "necro.client.MultiInstance"
local playerList = require "necro.client.PlayerList"
local serverPlayerList = require "necro.server.ServerPlayerList"
local serverSocket = require "necro.server.ServerSocket"
local settings = require "necro.config.Settings"
local settingsMenu = require "necro.menu.settings.SettingsMenu"
local settingsStorage = require "necro.config.SettingsStorage"
local spectator = require "necro.game.character.Spectator"
local textFormat = require "necro.config.i18n.TextFormat"
local tick = require "necro.cycles.Tick"

local enum = require "system.utils.Enum"
local utils = require "system.utils.Utilities"

hologramConfigMenuOpen = false
lastKnownTeamCount = 2
instanceIndex = nil

racingHologram.Mode = enum.sequence {
	DISABLED = enum.entry(0, {}),
	DOCKED = enum.entry(1, {
		enabled = true,
		external = false,
	}),
	POP_OUT = enum.entry(2, {
		name = "Pop-out",
		enabled = true,
		external = true,
	}),
}

racingHologram.HUDVisibility = enum.sequence {
	NEVER   = enum.entry(0, { effectiveVisibility = false }),
	DOCKED  = enum.entry(1, { effectiveVisibility = multiInstance.isEmbedded(), name = "Docked mode" }),
	POP_OUT = enum.entry(2, { effectiveVisibility = not multiInstance.isEmbedded(), name = "Pop-out mode" }),
	ALWAYS  = enum.entry(3, { effectiveVisibility = true }),
}

settings.group {
	id = "hologram",
	name = "Opponent view",
	autoRegister = true,
}

racingHologramMode = settings.user.enum {
	id = "hologram.mode",
	name = "Display mode",
	enum = racingHologram.Mode,
	default = racingHologram.Mode.DOCKED,
	order = 100,
	setter = function ()
		if hologramConfigMenuOpen then
			racingHologram.setViewCount(0)
			racingHologram.updateViewCount()
		end
	end,
}

autoClose = settings.user.bool {
	id = "hologram.autoClose",
	name = "Auto-close after round ends",
	default = true,
	order = 110,
}

settings.user.action {
	id = "hologram.hudVisisibility",
	name = "HUD visibility",
	action = function ()
		local mode = racingHologramMode == racingHologram.HUDVisibility.POP_OUT
			and racingHologram.HUDVisibility.POP_OUT or racingHologram.HUDVisibility.DOCKED
		menu.open("SynchronyRacing_HologramSettings", { flag = mode })
	end,
	autoRegister = true,
	order = 120,
}

racingHologramDefaultsSet = settings.user.table {
	visibility = settings.Visibility.HIDDEN,
}

local settingsDefaults = {
	["video.hud.scaleFactor"] = 2,
	["video.hud.stackSizeScale"] = 2,
	["video.hud.mirrorMode"] = hud.MirrorMode.REFLECTED,
	["display.scaleFactor"] = 2,
	["display.scaleMode"] = gameWindow.ScaleMode.AUTO,
	["misc.integrations.richPresence.discord"] = false,
}

activeHolograms = {}
closedInstances = {}

local initDone = false
local visibilitySettingsKeys = {}
local visibilitySettings = {}

visibilitySettings, initDone = script.persist(function ()
	return visibilitySettings, initDone
end)

function racingHologram.isEnabled()
	return racingConfig.getClientValue(racingConfig.Setting.ALLOW_HOLOGRAM) or racingSpectator.isClientBroadcaster()
end

function racingHologram.open(external)
	if not multiInstance.isDuplicate() and racingHologram.isEnabled() then
		local index = #activeHolograms + 1
		activeHolograms[#activeHolograms + 1] = multiInstance.create {
			external = external,
			windowTitle = "NDScreenwatch" .. tostring(index > 1 and index or ""),
		}
	end
end

function racingHologram.setViewCount(count)
	count = utils.clamp(0, count or 0, 4)

	if #activeHolograms > count then
		for _ = #activeHolograms - 1, count, -1 do
			racingHologram.close()
		end
	end

	if #activeHolograms < count and not multiInstance.isDuplicate() and racingHologram.isEnabled() then
		for _ = #activeHolograms + 1, count do
			racingHologram.open(racingHologram.Mode.data[racingHologramMode].external)
		end
	end
end

function racingHologram.getViewCount()
	return #activeHolograms
end

local function getTargetViewCount()
	if multiInstance.isDuplicate() or not racingHologram.isEnabled() or not racingHologram.Mode.data[racingHologramMode].enabled then
		return 0
	end

	local count = utils.clamp(0, (tonumber(racingProtocol.clientGetRoomAttribute(racingProtocol.RoomAttribute.TEAM_COUNT)) or 0) - 1, 4)

	-- Make sure at least one window is open for config preview
	if hologramConfigMenuOpen then
		count = math.max(count, 1)
	end

	-- Keep open between rounds, or in broadcaster mode
	if not autoClose or racingSpectator.isClientBroadcaster() then
		count = math.max(count, lastKnownTeamCount - 1)
	end

	-- Only allow 1 spectator view when not broadcasting
	if not racingSpectator.isClientBroadcaster() then
		count = math.min(count, 1)
	end

	return count
end

function racingHologram.updateViewCount()
	racingHologram.setViewCount(getTargetViewCount())
end

function racingHologram.isScreenWatchClient()
	return multiInstance.isDuplicate() and spectator.isSpectating(playerList.getLocalPlayerID())
end

function racingHologram.getInstanceIndex()
	if multiInstance.isDuplicate() then
		return instanceIndex
	else
		return 1
	end
end

autoCloseByMenu = tick.delay(function ()
	if menu.isOpen() and menu.getName() ~= "pause" then
		return false
	end
	hologramConfigMenuOpen = false
	racingHologram.updateViewCount()
end)

function racingHologram.openConfigMenu()
	settingsMenu.open {
		prefix = "mod.SynchronyRacing.hologram",
		title = "Opponent view",
	}

	hologramConfigMenuOpen = true
	autoCloseByMenu()
	racingHologram.updateViewCount()
end

local function tryCloseInstance(instance)
	if gameClient.isConnected() and instance.isOpen() then
		gameClient.sendReliable(racingProtocol.MessageType.HOLOGRAM, { selfDestruct = instance.uid })
		return false
	else
		return true
	end
end

local function tryCloseAllInstances()
	if closedInstances[1] then
		utils.removeIf(closedInstances, tryCloseInstance)
	end
end

function racingHologram.close()
	local activeHologram = table.remove(activeHolograms)
	if activeHologram then
		-- Add to "closed instances" list
		closedInstances[#closedInstances + 1] = activeHologram
		racingHologram.send { selfDestruct = activeHologram.uid }
		activeHologram.close()
	end
end

function racingHologram.send(message)
	if gameClient.isConnected() then
		gameClient.sendReliable(racingProtocol.MessageType.HOLOGRAM, message)
	end
end

updateHologramSettings = tick.delay(function ()
	local values = {}
	local spectatorOverride = racingSpectator.isClientBroadcaster() and racingHologram.HUDVisibility.ALWAYS
	for _, key in ipairs(visibilitySettingsKeys) do
		values[key] = spectatorOverride or settingsStorage.get(key)
	end
	racingHologram.send { settings = values }
end)

event.clientRoomAttribute.add("updateRacingHologram", racingProtocol.RoomAttribute.TEAM_COUNT, function (ev)
	local teamCount = tonumber(ev.value) or 0
	if teamCount > 0 then
		lastKnownTeamCount = teamCount
	end
	racingHologram.updateViewCount()
end)

local function updateHologramSettingsIfNecessary(playerID)
	if playerList.getAttribute(playerID, racingProtocol.PlayerAttribute.PARENT) == playerList.getLocalPlayerID()
		and playerList.getAttribute(playerID, racingProtocol.PlayerAttribute.INITIALIZED)
	then
		updateHologramSettings()
		for i = 1, #activeHolograms do
			racingHologram.send { uid = activeHolograms[i].uid, setIndex = i + 1 }
		end
		tryCloseAllInstances()
	end
end

event.clientPlayerList.add("syncHologramSettingsOnParent", racingProtocol.PlayerAttribute.PARENT, function (ev)
	updateHologramSettingsIfNecessary(ev.playerID)
end)

event.clientPlayerList.add("syncHologramSettingsOnInit", racingProtocol.PlayerAttribute.INITIALIZED, function (ev)
	updateHologramSettingsIfNecessary(ev.playerID)
end)

customActions.registerHotkey {
	id = "SPECTATE_CYCLE",
	name = "Cycle opponent view",
	keyBinding = "pagedown",
	enableIf = function ()
		return racingHologram.getViewCount() == 1
	end,
	callback = function ()
		gameClient.sendReliable(racingProtocol.MessageType.SPECTATE_CYCLE, {})
	end,
}

local invalidMenus = {
	changeLog = true,
	bestiary = true,
}

event.tick.add("initRacingHologramOptions", "lobby", function (ev)
	if menu.isOpen() and multiInstance.isDuplicate() and invalidMenus[menu.getName()] then
		menu.close()
	end

	if not initDone and gameClient.isLoggedIn() then
		initDone = true
		if not multiInstance.isDuplicate() then
			return
		end

		local anyChanged = false
		for key, value in pairs(settingsDefaults) do
			if not racingHologramDefaultsSet[key] then
				settingsStorage.set(key, value)
				racingHologramDefaultsSet[key] = true
				anyChanged = true
			end
		end
		if anyChanged then
			settingsStorage.saveToFile()
		end

		playerList.setAttribute(racingProtocol.PlayerAttribute.INITIALIZED, true)
	end
end)

local function viewFlag(name, handlers, args)
	local id = type(handlers) == "string" and handlers or handlers[1][2]
	local def = {
		id = "hologram.hud." .. id,
		name = name,
		enum = racingHologram.HUDVisibility,
		default = racingHologram.HUDVisibility.POP_OUT,
		autoRegister = true,
		visibility = settings.Visibility.HIDDEN,
		setter = function (value)
			if multiInstance.isDuplicate() then
				visibilitySettings[id] = racingHologram.HUDVisibility.data[value].effectiveVisibility
			else
				updateHologramSettings()
			end
		end,
		order = 1000 + #visibilitySettingsKeys,
	}
	if args then
		utils.mergeTables(def, args)
	end
	visibilitySettingsKeys[#visibilitySettingsKeys + 1] = settings.user.enum(def)
	if multiInstance.isDuplicate() and type(handlers) == "table" then
		for _, handler in ipairs(handlers) do
			handler[1].override(handler[2], 1, function (func, ev)
				if visibilitySettings[id] then
					return func(ev)
				end
			end)
		end
	end
end

function racingHologram.getElementVisibility(id)
	return visibilitySettings[id]
end

local alwaysVisible = { default = racingHologram.HUDVisibility.ALWAYS }
local neverVisible = { default = racingHologram.HUDVisibility.NEVER }

viewFlag("Show health bar",         {{event.renderPlayerHUD, "renderHealthBar"}}, alwaysVisible)
viewFlag("Show items",              {{event.renderPlayerHUD, "renderEquippedItems"}, {event.renderGlobalHUD, "renderSharedItems"}}, alwaysVisible)
viewFlag("Show item slot borders",  {{event.inventoryHUDRenderSlot, "itemSlotImage"}}, alwaysVisible)
viewFlag("Show item slot labels",   {{event.inventoryHUDRenderSlot, "itemSlotHeaderLabel"}}, alwaysVisible)
viewFlag("Show gold counter",       {{event.renderPlayerHUD, "goldCounter"}, {event.renderPlayerHUD, "diamondCounter"}})
viewFlag("Show coin multiplier",    {{event.renderPlayerHUD, "renderGrooveChain"}, {event.renderGlobalHUD, "renderGrooveChain"}})
viewFlag("Show beat bars",          {{event.renderGlobalHUD, "renderBeatIndicators"}, {event.renderGlobalHUD, "renderHeartbeatHUD"}})
viewFlag("Show minimap",            {{event.renderGlobalHUD, "renderMinimap"}}, alwaysVisible)
viewFlag("Show boss intro",         {{event.renderGlobalHUD, "bossSplash"}}, alwaysVisible)
viewFlag("Show level counter",      {{event.renderGlobalHUD, "renderLevelCounter"}})
viewFlag("Show player hearts",      "showPlayerHearts", neverVisible)
viewFlag("Show player name tags",   {{event.render, "renderPlayerNames"}}, neverVisible)
viewFlag("Show player list",        {{event.renderGlobalHUD, "renderPlayerList"}})
viewFlag("Show spectator controls", {{event.renderGlobalHUD, "renderSpectatorHUD"}})
viewFlag("Show speedrun timer",     "showSpeedrunTimer")
viewFlag("Show countdowns",         {{event.renderUI, "drawCountdown"}})
viewFlag("Show item key bindings",  {{event.inventoryHUDRenderSlot, "keyBindings"}})
viewFlag("Show opponent progress",  "showOpponentProgress", neverVisible)
viewFlag("Show screen overlays",    {{event.render, "drawOverlays"}})
viewFlag("Show flyaway text",       {{event.render, "renderFlyaways"}})
viewFlag("Show chat messages",      {{event.renderUI, "renderChat"}})
viewFlag("Show floor tiles",        {{event.render, "renderFloors"}}, alwaysVisible)
viewFlag("Show wall tiles",         {{event.render, "renderWalls"}}, alwaysVisible)
--viewFlag("Show character selector", {{event.renderUI, "renderLobby"}})

event.menu.add("hologramSettings", "SynchronyRacing_HologramSettings", function (ev)
	ev.arg = ev.arg or {}
	local flag = ev.arg.flag or 0
	local entries = {}
	for _, key in ipairs(visibilitySettingsKeys) do
		entries[#entries + 1] = {
			label = string.format("%s %s", settingsStorage.getName(key), textFormat.checkbox(bit.band(flag, settingsStorage.get(key) or 0) ~= 0)),
			action = function ()
				settingsStorage.set(key, bit.bxor(flag, settingsStorage.get(key) or 0))
				menu.update()
			end,
		}
	end

	local function toggleFlag()
		ev.arg.flag = (flag == racingHologram.HUDVisibility.DOCKED)
			and racingHologram.HUDVisibility.POP_OUT
			or racingHologram.HUDVisibility.DOCKED
		menu.update()
	end

	entries[#entries + 1] = { height = 0 }
	entries[#entries + 1] = {
		label = string.format("Edit config for: %s", racingHologram.HUDVisibility.data[flag].name),
		action = toggleFlag,
		leftAction = toggleFlag,
		rightAction = toggleFlag,
	}

	entries[#entries + 1] = { height = 0 }
	entries[#entries + 1] = { label = "Done", action = menu.close, sound = "UIBack" }
	ev.menu = {
		entries = entries,
		label = "HUD visibility",
		mouseControl = false,
	}
end)

event.menu.add("hologramSettingsMouseControlWorkaround", {sequence = 1, key = "settings"}, function (ev)
	if ev.menu and ev.arg and ev.arg.prefix == "mod.SynchronyRacing.hologram" then
		ev.menu.mouseControl = false
	end
end)

event.serverMessage.add("hologramCommand", racingProtocol.MessageType.HOLOGRAM, function (ev)
	for _, playerID in ipairs(serverPlayerList.allPlayers()) do
		if serverPlayerList.getAttribute(playerID, racingProtocol.PlayerAttribute.PARENT) == ev.playerID then
			serverSocket.sendReliable(racingProtocol.MessageType.HOLOGRAM, ev.message, playerID)
		end
	end
end)

event.clientMessage.add("hologramCommand", racingProtocol.MessageType.HOLOGRAM, function (ev)
	if multiInstance.isDuplicate() then
		if ev.uid and ev.uid ~= multiInstance.getSessionUID() then
			return
		end
		if ev.setIndex then
			instanceIndex = tonumber(ev.setIndex) or 1
		end
		if ev.selfDestruct == multiInstance.getSessionUID() then
			return gameClient.disconnect()
		end
		if ev.settings then
			for key, value in pairs(ev.settings) do
				settingsStorage.set(key, value)
			end
		end
	end
end)

return racingHologram
