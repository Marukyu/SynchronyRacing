local racingConfig = {}

local racingMatch = require "SynchronyRacing.RacingMatch"
local racingProtocol = require "SynchronyRacing.RacingProtocol"
local racingRooms = require "SynchronyRacing.RacingRooms"
local cutscene = require "necro.client.Cutscene"

local event = require "necro.event.Event"
local gameClient = require "necro.client.GameClient"
local gameSession = require "necro.client.GameSession"
local itemGeneration = require "necro.game.item.ItemGeneration"
local menu = require "necro.menu.Menu"
local netplay = require "necro.network.Netplay"
local remoteSettings = require "necro.config.RemoteSettings"
local resources = require "necro.client.Resources"
local room = require "necro.client.Room"
local settings = require "necro.config.Settings"
local settingsPresets = require "necro.config.SettingsPresets"
local settingsStorage = require "necro.config.SettingsStorage"

local serverResources = require "necro.server.ServerResources"
local serverRooms = require "necro.server.ServerRooms"

local enum = require "system.utils.Enum"
local try = require "system.utils.Try"
local utils = require "system.utils.Utilities"

local function confKey(name)
	return "mod.SynchronyRacing.racingConfig." .. name
end

local modListUpdatePending = true
local autoSettingsPending = true

local autoSettings = {
    ["camera.enforceZoomLimit"] = true,
    ["camera.zoomFactor"] = 2,
    ["gameplay.markSeen"] = itemGeneration.Mode.RACING,
	["gameplay.rng.forceSeeded"] = true,
	["gameplay.bossSplash"] = true,
	["gameplay.lateJoin.enabled"] = false,
	["video.cutscene.displayModeStory"] = cutscene.DisplayMode.NEVER,
	["video.cutscene.displayModeCredits"] = cutscene.DisplayMode.NEVER,
	["mod.SynchronyRacing.bannedItems"] = {
		FeetBootsLeaping = true,
		FeetBootsLunging = true,
		Sync_WeaponOnyxAxe = true,
		WeaponAxe = true,
		WeaponBloodAxe = true,
		WeaponGlassAxe = true,
		WeaponGoldenAxe = true,
		WeaponObsidianAxe = true,
		WeaponObsidianAxeUncertain = true,
		WeaponTitaniumAxe = true,
		WeaponTitaniumAxeUncertain = true,
	},
    ["mod.SynchronyRacing.racingGhost.showGhost"] = false,
    ["mod.SynchronyRacing.racingGhost.showGhostCompass"] = false,
	["input.lobbySkipIdle"] = true,
	["lobby.ignoreRhythm"] = false,
}

racingConfig.Setting = {
	MOD_LIST = confKey "modList",
	MOD_LIST_ENABLE_PER_ROOM = confKey "modListEnablePerRoom",
	GAME_MODE = confKey "gameMode",
	TEAM_MODE = confKey "teamMode",
	START_COUNTDOWN = confKey "startCountdown",
	TIME_LIMIT = confKey "timeLimit",
	AUTO_PAUSE = confKey "autoPause",
	AUTO_START = confKey "autoStart",
	AUTO_CLOSE = confKey "autoClose",
	AUTO_CLOSE_MODE = confKey "autoCloseMode",
	AUTO_CLOSE_CUTOFF = confKey "autoCloseCutoff",
	VICTORY_MODE = confKey "victoryMode",
	ALLOW_FORFEIT = confKey "allowForfeit",
	FORFEIT_DELAY = confKey "forfeitDelay",
	AUTO_SPECTATE = confKey "autoSpectate",
	ALLOW_HOLOGRAM = confKey "allowHologram",

	ENSEMBLE_PREP_TIME_MIN = confKey "ensemble.prepTime.min",
	ENSEMBLE_PREP_TIME_MAX = confKey "ensemble.prepTime.max",
	ENSEMBLE_START_DELAY = confKey "ensemble.startDelay",
}

modList = settings.shared.table {
	format = function (value)
		return L.formatKey("%s mods", "racingConfig.modCountDisplay", type(value) == "table" and #value or 0)
	end,
}

modListEnablePerRoom = settings.shared.bool {
	name = "Enable per-room mod list",
	desc = "Only enable this if you want to force a specific mod setup for each race without affecting the main lobby.",
	order = 50,
}

gameMode = settings.shared.table {
	default = {mode = "AllZonesSeeded"},
}

gameModeSelect = settings.shared.action {
	name = function ()
		local mode = settingsStorage.get(racingConfig.Setting.GAME_MODE, settings.Layer.REMOTE_PENDING)
			or settingsStorage.get(racingConfig.Setting.GAME_MODE, settings.Layer.REMOTE_OVERRIDE)
			or settingsStorage.get(racingConfig.Setting.GAME_MODE, settings.Layer.DEFAULT)
		local modeID = mode and mode.mode
		local modeName = gameSession.Mode.data[modeID].name or modeID or "<Unknown>"
		local fileName = mode and mode.fileName
		if type(fileName) == "string" then
			return string.format("%s: %s", modeName, string.match(fileName, "[^/]+$") or fileName)
		else
			return string.format("Game mode: %s", modeName)
		end
	end,
	action = function ()
		menu.open("lobbyModeSelector", {
			selectCallback = function (modeData)
				settingsStorage.set(racingConfig.Setting.GAME_MODE, modeData, settings.Layer.REMOTE_PENDING)
			end,
		})
	end,
	enableIf = function ()
		return remoteSettings.hasPermissions()
	end,
	order = 0,
}

startCountdown = settings.shared.time {
	name = "Race start countdown",
	step = 1,
	default = 10,
	minimum = 0,
	maximum = 10,
	order = 9,
}

timeLimit = settings.shared.time {
	name = "Race time limit",
	step = 30,
	default = 60 * 15,
	order = 10,
}

teamMode = settings.shared.enum {
	name = "Team mode",
	enum = racingMatch.TeamMode,
	default = racingMatch.TeamMode.SPLIT_ROOMS,
	order = 11,
}

racingConfig.VictoryMode = enum.sequence {
	NONE = 0,
	TIME = 1,
	SCORE = 2,
}

victoryMode = settings.shared.enum {
	name = "Victory mode",
	enum = racingConfig.VictoryMode,
	default = racingConfig.VictoryMode.TIME,
	order = 12,
}

allowForfeit = settings.shared.bool {
	name = "Allow player resignation",
	default = true,
	order = 16,
}

forfeitDelay = settings.shared.time {
	name = "Allow post-death resign after",
	desc = "Time that must have passed in the race before the 'Resign' option is shown on the post-death replay menu.",
	default = 10 * 60,
	order = 17,
	step = 30,
	minimum = 0,
	maximum = 30 * 60,
}

autoPause = settings.shared.bool {
	name = "Auto-pause on disconnect",
	default = false,
	order = 40,
}

autoStart = settings.shared.bool {
	name = "Auto-start when all players ready",
	default = true,
	order = 41,
}

autoClose = settings.shared.bool {
	name = "Auto-close race on finish/forfeit",
	default = true,
	order = 42,
}

racingConfig.AutoCloseMode = enum.sequence {
	ALL = 0,
	LAST = 1,
}

autoCloseMode = settings.shared.enum {
	name = "Auto-close mode",
	enum = racingConfig.AutoCloseMode,
	default = racingConfig.AutoCloseMode.ALL,
	order = 42,
}

autoCloseCutoff = settings.shared.number {
	name = "Auto-close level cutoff",
	default = 20,
	maximum = 22,
	minimum = 1,
	order = 43,
}

autoSpectate = settings.shared.bool {
	name = "Auto-spectate active race",
	desc = "Automatically places late-joining players in spectator mode for an ongoing race",
	default = false,
	order = 44,
}

allowHologram = settings.shared.bool {
	name = "Enable opponent view",
	default = true,
	order = 50,
}

settings.group {
	id = "racingConfig.ensemble",
	name = "Ensemble mode",
	order = 60,
	autoRegister = true,
}

ensemblePrepTimeMin = settings.shared.time {
	id = "racingConfig.ensemble.prepTime.min",
	name = "Ensemble minimum preparation time",
	desc = "Minimum time racers must take to select their Ensemble characters before being allowed to ready up.",
	default = 15,
	step = 1,
	minimum = 0,
	maximum = 90,
	order = 10,
}

ensemblePrepTimeMax = settings.shared.time {
	id = "racingConfig.ensemble.prepTime.max",
	name = "Ensemble maximum preparation time",
	desc = "Time after which an Ensemble race force-starts, choosing a random character order to fill any empty spaces.",
	default = 60,
	step = 5,
	minimum = 5,
	maximum = 300,
	order = 20,
}

ensembleStartDelay = settings.shared.time {
	id = "racingConfig.ensemble.startDelay",
	name = "Ensemble start countdown",
	desc = "Time to wait before starting the race once all participants have selected their Ensemble order. This allows viewers and racers to review the chosen character order ahead of time.",
	default = 15,
	step = 1,
	minimum = 0,
	maximum = 60,
	order = 30,
}

local function copyResource(sourceRoomID, targetRoomID, resourceID)
	local resource = serverResources.getResource(sourceRoomID, resourceID)
	if resource then
		serverResources.load(targetRoomID, resourceID, resource.name, resource.data)
	end
end

local function getSettingsFromPreset(presetName)
	local preset = presetName ~= "" and try.getOrNil(settingsPresets.getSettings, presetName)
	if type(preset) ~= "table" then
		preset = serverResources.getResourceData(serverRooms.getDefaultRoomID(), netplay.Resource.GAME_SETTINGS)
	end
	return preset
end

function racingConfig.getAllValuesForPreset(presetName)
	local values = getSettingsFromPreset(presetName)
	local deserialize = settingsStorage.deserialize
	for key, value in pairs(values) do
		values[key] = deserialize(key, value)
	end
	return values
end

function racingConfig.getValueForPreset(presetName, key)
	local value = (getSettingsFromPreset(presetName) or {})[key]
	if value ~= nil then
		return settingsStorage.deserialize(key, value)
	else
		return utils.fastCopy(settingsStorage.getDefaultValue(key))
	end
end

--- Placeholder object for resetting a settings value back to nil
--- @class RacingConfig.Default

--- @type RacingConfig.Default
racingConfig.DEFAULT = (function () end)

local function resolveDiffValue(key, value)
	if type(value) == "function" then
		if value == racingConfig.DEFAULT then
			-- Reset to default (fast path)
			return nil
		else
			-- Apply function
			value = value()
		end
	end
	return settingsStorage.serialize(key, value)
end

local function applySettingsDiff(roomSettings, settingsDiff)
	local modified = false
	if type(roomSettings) ~= "table" then
		roomSettings = {}
	end
	if type(settingsDiff) == "table" then
		for key, value in pairs(settingsDiff) do
			if type(key) == "string" then
				local success, result = pcall(resolveDiffValue, key, value)
				if success and not utils.deepEquals(roomSettings[key], result) then
					if not modified then
						roomSettings = utils.fastCopy(roomSettings)
						modified = true
					end
					roomSettings[key] = result
				end
			end
		end
	end
	return roomSettings, modified
end

function racingConfig.modifyRoomSettings(roomID, settingsDiff)
	if racingRooms.isRacingRoom(roomID) and type(settingsDiff) == "table" then
		local settingsResource = serverResources.getResource(roomID, netplay.Resource.GAME_SETTINGS) or {}
		local roomSettings = settingsResource.data
		if type(roomSettings) == "table" then
			roomSettings = utils.fastCopy(roomSettings)
		else
			roomSettings = {}
		end
		local modifiedSettings, modified = applySettingsDiff(roomSettings, settingsDiff)
		if modified then
			serverResources.load(roomID, netplay.Resource.GAME_SETTINGS, settingsResource.name, modifiedSettings)
		end
	end
end

function racingConfig.applyRoomSettings(roomID, settingsTable)
	if type(settingsTable) ~= "table" then
		settingsTable = {}
	else
		local serialize = settingsStorage.serialize
		for key, value in pairs(settingsTable) do
			settingsTable[key] = serialize(key, value)
		end
	end

	if type(settingsTable[racingConfig.Setting.MOD_LIST]) == "table"
		and settingsTable[racingConfig.Setting.MOD_LIST_ENABLE_PER_ROOM]
	then
		serverResources.load(roomID, netplay.Resource.MOD_LIST, "", settingsTable[racingConfig.Setting.MOD_LIST])
	else
		copyResource(serverRooms.getDefaultRoomID(), roomID, netplay.Resource.MOD_LIST)
	end

	serverResources.load(roomID, netplay.Resource.GAME_SETTINGS, "", settingsTable)
end

function racingConfig.getValueForRoom(roomID, key)
	-- TODO clean up this function
	-- This tries to first retrieve the config value in the room's settings, falling back to the main room, then the
	-- setting's default value
	local value
	if serverRooms.isValidRoom(roomID) then
		local roomSettings = serverResources.getResourceData(roomID, netplay.Resource.GAME_SETTINGS)
		if type(roomSettings) == "table" and roomSettings[key] ~= nil then
			value = settingsStorage.deserialize(key, roomSettings[key])
		end
		if value ~= nil then
			return value
		end
	end
	value = settingsStorage.get(key, settings.Layer.REMOTE_OVERRIDE)
	if value ~= nil then
		return value
	end
	return settingsStorage.getDefaultValue(key)
end

function racingConfig.getValueForMatch(match, key)
	local team = match.teams[1]
	return racingConfig.getValueForRoom(team and team.room, key)
end

function racingConfig.listPresets()
	return settingsPresets.list()
end

function racingConfig.createPreset(name, data)
	if data then
		local originalSettings = settingsStorage.getSerializedSettings(settings.Layer.REMOTE_PENDING)
		settingsStorage.overrideSerializedSettings(settings.Layer.REMOTE_PENDING, data)
		try.catch(settingsPresets.save, name, settings.Layer.REMOTE_PENDING)
		settingsStorage.overrideSerializedSettings(settings.Layer.REMOTE_PENDING, originalSettings)
	end
end

function racingConfig.deletePreset(name)
	return settingsPresets.delete(name)
end

function racingConfig.getClientValue(key)
	return utils.fastCopy(settingsStorage.get(key))
end

event.resourceLoaded.add("updateModConfig", "MOD_LIST", function (ev)
	modListUpdatePending = true
end)

event.menu.add("updateModListOnPresetMenu", "settingsPresets", function (ev)
	if not ev.update then
		modListUpdatePending = true
	end
end)

local function initAutoSettings()
	if room.getAttribute(racingProtocol.RoomAttribute.ROOM_TYPE) ~= racingRooms.RoomType.RACE then
		local anyChanged = false
		local layer = settings.Layer.REMOTE_PENDING
		for key, value in pairs(autoSettings) do
			if settingsStorage.get(key, layer) == nil then
				settingsStorage.set(key, value, layer)
				anyChanged = true
			end
		end
		if anyChanged then
			remoteSettings.upload()
		end
	end
end

event.serverReset.add("resetRacingAutoSettings", "playerList", function (ev)
	autoSettingsPending = true
end)

event.serverTick.add("initRacingAutoSettings", {order = "conductor", sequence = -2}, function (ev)
	if autoSettingsPending and gameClient.isLoggedIn() and resources.isResourceListReady() then
		autoSettingsPending = false
		initAutoSettings()
	end
end)

event.tick.add("updateModConfig", "gameContent", function (ev)
	if modListUpdatePending then
		modListUpdatePending = false
		if room.getAttribute(racingProtocol.RoomAttribute.ROOM_TYPE) ~= racingRooms.RoomType.RACE then
			local mods = resources.getData(netplay.Resource.MOD_LIST)
			if type(mods) == "table" then
				settingsStorage.set(racingConfig.Setting.MOD_LIST, utils.fastCopy(mods), settings.Layer.REMOTE_PENDING)
			end
		end
	end
end)

return racingConfig
