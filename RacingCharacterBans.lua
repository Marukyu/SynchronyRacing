local components = require "necro.game.data.Components"
local ecs = require "system.game.Entities"
local event = require "necro.event.Event"
local menu = require "necro.menu.Menu"
local player = require "necro.game.character.Player"
local remoteSettings = require "necro.config.RemoteSettings"
local settings = require "necro.config.Settings"
local sound = require "necro.audio.Sound"

local color = require "system.utils.Color"
local utils = require "system.utils.Utilities"

local settingName = "mod.SynchronyRacing.allowedPlayerCharacters"

allowedPlayerCharacters = settings.entitySchema.table {
	id = "allowedPlayerCharacters",
	default = {Cadence = true},
}

editAllowedCharacters = settings.shared.action {
	name = function ()
		local chars = utils.getKeyList(remoteSettings.getLocalOverrideValue(settingName) or allowedPlayerCharacters)
		pcall(table.sort, chars)
		return string.format("Allowed characters: %s", table.concat(chars, ", "))
	end,
	order = 1,
	action = function ()
		menu.open("SynchronyRacing_CharacterSelector")
	end,
}

local function setEnabled(typeName, enabled)
	if remoteSettings.hasPermissions() then
		local setting = utils.fastCopy(remoteSettings.getLocalOverrideValue(settingName) or allowedPlayerCharacters) or {}
		setting[typeName] = enabled or nil
		remoteSettings.setLocalOverrideValue(settingName, setting)
	end
end

local function isEnabled(typeName)
	local setting = remoteSettings.getLocalOverrideValue(settingName) or allowedPlayerCharacters
	return not setting or setting[typeName]
end

local function getIcon(entity, i, enabledFunc)
	local sprite = entity.sprite
	local icon = {
		image = sprite.texture,
		imageRect = {sprite.textureShiftX, sprite.textureShiftY, sprite.width, sprite.height},
		width = sprite.width,
		height = sprite.height,
	}
	return function ()
		local selected, enabled = menu.getSelectedID() == i, enabledFunc()
		icon.color = color.hsv(0, 0, (selected and 1 or 0.8) * (enabled and 1 or 0.5))
		return icon
	end
end

local function getEntry(entity, x, y, i, enabledFunc)
	return {
		x = x,
		y = y,
		icon = getIcon(entity, i, enabledFunc),
		selectableIf = function () end,
		action = function () end,
	}
end

local function moveEntry(x, y, off)
	return x + (off and off.offsetX or 0), y + (off and off.offsetY or 0)
end

local function setAllEnabled(enabled)
	for _, entity in ecs.prototypesWithComponents {"playableCharacter"} do
		setEnabled(ecs.getEntityTypeName(entity), enabled)
	end
end

event.menu.add("SynchronyRacingCharacterSelector", "SynchronyRacing_CharacterSelector", function (ev)
	local entities = {}

	local _, playerType = pcall(ecs.getEntityTypeName, player.getPlayerEntity())

	for _, entity in ecs.prototypesWithComponents {"playableCharacter"} do
		entities[#entities + 1] = entity
	end

	table.sort(entities, function (e1, e2)
		return e1.playableCharacter.lobbyOrder < e2.playableCharacter.lobbyOrder
	end)

	local entries = {}

	for i, entity in ipairs(entities) do
		local function menuOffset(diff)
			return function ()
				if i + diff <= #entities and i + diff > 0 then
					menu.selectByID(i + diff)
				elseif diff > 0 then
					menu.selectByID("_enableAll")
				elseif diff < 0 then
					menu.selectByID("_done")
				end
				sound.playUI(diff > 0 and "UISelectDown" or "UISelectUp")
			end
		end

		local x = ((i - 1) % 10 - 4.5) * 48
		local y = math.floor((i - 1) / 10) * 48
		local typeName = ecs.getEntityTypeName(entity)

		local function isThisEntityEnabled()
			return isEnabled(typeName)
		end

		if entity.sprite then
			entries[#entries + 1] = getEntry(entity, x, y, i, isThisEntityEnabled)
		end
		local head = entity.characterWithAttachment
			and ecs.getEntityPrototype(entity.characterWithAttachment.attachmentType)
		if head and head.sprite then
			x, y = moveEntry(x, y, head.attachmentCopySpritePosition)
			entries[#entries + 1] = getEntry(head, x, y, i, isThisEntityEnabled)
		end

		entries[#entries + 1] = {
			id = i,
			x = x,
			y = y,
			label = not entity.sprite and typeName or "",
			action = function ()
				setEnabled(typeName, not isEnabled(typeName))
			end,
			specialAction = function ()
				setAllEnabled(false)
				setEnabled(typeName, true)
			end,
			upAction = menuOffset(-10),
			downAction = menuOffset(10),
			rightAction = menuOffset(1),
			leftAction = menuOffset(-1),
			rightSound = "",
			leftSound = "",
			selected = playerType == typeName,
		}
	end

	entries[#entries + 1] = {height = 0}

	entries[#entries + 1] = {
		id = "_enableAll",
		label = "Reset",
		action = function ()
			setAllEnabled(true)
		end,
	}

	entries[#entries + 1] = {
		id = "_done",
		label = "Done",
		action = function ()
			menu.close()
			menu.update()
		end,
		sound = "UIBack",
	}

	ev.menu = {
		label = "Allowed characters",
		entries = entries,
		directionalConfirmation = false,
		escapeAction = function ()
			menu.close()
			menu.update()
		end,
	}
end)

event.entitySchemaLoadEntity.add("charBans", {order = "overrides"}, function (ev)
	if ev.entity.playableCharacter and not allowedPlayerCharacters[ev.entity.name] then
		ev.entity.playableCharacterNonSelectable = {}
	end
end)
