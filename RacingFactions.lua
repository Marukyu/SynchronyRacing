local racingFactions = {}

local racingMatch = require "SynchronyRacing.RacingMatch"
local racingProtocol = require "SynchronyRacing.RacingProtocol"
local racingRooms = require "SynchronyRacing.RacingRooms"

local currentLevel = require "necro.game.level.CurrentLevel"
local event = require "necro.event.Event"
local gameClient = require "necro.client.GameClient"
local playerList = require "necro.client.PlayerList"
local resources = require "necro.client.Resources"
local serverPlayerList = require "necro.server.ServerPlayerList"
local snapshot = require "necro.game.system.Snapshot"
local ui = require "necro.render.UI"

local color = require "system.utils.Color"
local utils = require "system.utils.Utilities"

racingFactions.Colors = {
	color.rgb(100, 220, 220),
	color.rgb(255, 100, 100),
	color.rgb(100, 250, 100),
	color.rgb(250, 250, 80),
	-- TODO more colors
}

local needScoreUpdate = true

factionScores = snapshot.runVariable({})

function racingFactions.getPlayerFaction(playerID)
	local factions = resources.getData(racingProtocol.Resource.FACTIONS)
	if type(factions) == "table" then
		return factions[playerID]
	end

	return racingProtocol.clientGetPlayerAttribute(playerID, racingProtocol.PlayerAttribute.FACTION)
		or racingProtocol.clientGetPlayerAttribute(playerID, racingProtocol.PlayerAttribute.TEAM)
end

function racingFactions.getScore(faction)
	return factionScores[faction] or 0
end

function racingFactions.setScore(faction, score)
	factionScores[faction] = score
	needScoreUpdate = true
end

function racingFactions.addScore(faction, score)
	factionScores[faction] = (factionScores[faction] or 0) + score
	needScoreUpdate = true
end

--- @param match SynchronyRacingMatch
function racingFactions.prepareMatch(match)
	match.mode = match.mode or racingMatch.TeamMode.SPLIT_ROOMS
	if match.mode == racingMatch.TeamMode.FREE_FOR_ALL then
		for _, team in ipairs(match.teams) do
			team.factions = {}
			for i, userID in ipairs(team.users) do
				team.factions[userID] = i
			end
		end
	elseif match.mode == racingMatch.TeamMode.SAME_ROOM then
		local globalTeam = {users = {}, factions = {}}
		for teamID, team in ipairs(match.teams) do
			for _, userID in ipairs(team.users) do
				table.insert(globalTeam.users, userID)
				globalTeam.factions[userID] = teamID
			end
		end
		match.teams = {globalTeam}
	end
	return match
end

event.renderPlayerListEntry.override("renderPing", 1, function (func, ev)
	if currentLevel.isLobby() then
		return func(ev)
	end

	local factionTable = resources.getData(racingProtocol.Resource.FACTIONS)
	local faction = factionTable and factionTable[ev.playerID]
	if faction then
		ev.textRect = ui.drawText {
			buffer = ev.buffer,
			font = ui.Font.SMALL,
			size = ui.Font.SMALL.size * ev.scale,
			text = string.format("%d  pts", tonumber(racingFactions.getScore(faction)) or 0),
			x = (ev.textRect.x or 0) + (ev.textRect.width or 0) + 8 * ev.scale,
			y = ev.slotRect[2] + ev.slotRect[4] - 1 * ev.scale,
			alignY = 1,
			fillColor = color.fade(racingFactions.Colors[faction] or color.WHITE, ev.opacity),
			shadowColor = color.TRANSPARENT,
			outlineColor = color.BLACK,
			outlineThickness = 1,
			uppercase = false,
			z = 0,
		}
	end
end)

event.tick.add("factionScores", "gameOver", function (ev)
	if needScoreUpdate then
		needScoreUpdate = false
		if playerList.isHost() and gameClient.isConnected() then
			racingProtocol.clientSendMessage(racingProtocol.MessageType.FACTION_SCORES, factionScores)
		end
	end
end)

event.serverMessage.add("factionScores", racingProtocol.MessageType.FACTION_SCORES, function (ev)
	if serverPlayerList.isHost(ev.playerID) and type(ev.message) == "table" then
		local roomID = serverPlayerList.getRoomID(ev.playerID)

		local racingRoom = racingRooms.getRoom(roomID)
		if not racingRoom then
			return
		end

		local match = racingRooms.getMatch(racingRoom.matchID)
		if not match or match.status ~= racingMatch.Status.ACTIVE then
			return
		end

		local team = match.teams[racingRoom.teamID]
		if not team then
			return
		end

		team.factionScores = ev.message
	end
end)


return racingFactions
