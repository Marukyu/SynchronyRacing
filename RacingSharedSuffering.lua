local player = require "necro.game.character.Player"
local playerList = require "necro.client.PlayerList"
local event = require "necro.event.Event"
local object = require "necro.game.object.Object"
local damage = require "necro.game.system.Damage"
local flyaway = require "necro.game.system.Flyaway"
local health = require "necro.game.character.Health"
local sound = require "necro.audio.Sound"
local settings = require "necro.config.Settings"
local components = require "necro.game.data.Components"

local ecs = require "system.game.Entities"

local field = components.field
local constant = components.constant

settings.prefixAll("sharedSuffering")

enableSharedDamage = settings.entitySchema.bool {
	name = "Enable shared damage",
	default = false,
	order = 20,
}

enableSharedDeath = settings.entitySchema.bool {
	name = "Enable shared death",
	default = true,
	order = 30,
}

components.register {
	SynchronyRacing_sharedSuffering = {
		field.bool("processing"),
	},
}

local handlingPlayerDeath = false
local lastHurtPlayer

event.gameStateReset.add("racingSharedSufferingReset", "init", function (ev)
	handlingPlayerDeath = false
end)

event.gameStateLevel.add("racingSharedSufferingReset", "init", function (ev)
	handlingPlayerDeath = false
	lastHurtPlayer = nil
end)

event.objectDeath.add("killAllPlayers", {filter = "controllable", order = "runState"}, function (ev)
	if enableSharedDeath and ev.entity.controllable.playerID ~= 0 and not handlingPlayerDeath then
		handlingPlayerDeath = true
		for _, entity in ipairs(player.getPlayerEntities()) do
			if entity.id ~= ev.entity.id then
				object.kill(entity, ev.entity, "Shared suffering", damage.Type.SUICIDE)
				if entity.controllable.playerID ~= lastHurtPlayer then
					local name = playerList.getName(lastHurtPlayer or ev.entity.controllable.playerID) or "Your partner"
					flyaway.create { entity = entity, text = name .. " died!" }
				end
			end
		end
		handlingPlayerDeath = false
	end
end)

local function findHealthiestEntity()
	local maxID, maxHealth = 0, 1
	for entity in ecs.entitiesWithComponents {"SynchronyRacing_sharedSuffering", "health"} do
		if not entity.SynchronyRacing_sharedSuffering.processing and entity.health.health > maxHealth
			and entity.gameObject.tangible
		then
			maxID = entity.id
			maxHealth = entity.health.health
		end
	end
	return ecs.getEntityByID(maxID)
end

local function inflictSharedSuffering(target, sufferer, amount, ev)
	if not target then
		return
	end

	health.heal { entity = sufferer, health = 1, silent = true }

	if target.invincibility then
		target.invincibility.remainingTurns = 0
	end

	damage.inflict {
		damage = amount,
		victim = target,
		attacker = sufferer,
		type = damage.Type.BLOOD,
	}

	flyaway.create { entity = target, text = "Shared suffering!" }
	flyaway.create { entity = sufferer, text = "Shared suffering!" }

	-- Sound is stereo, so audible from anywhere
	sound.play("ringofpain", target.position.x, target.position.y, {volume = 1, pitch = 0.8})
end

event.objectTakeDamage.add("sharedSuffering",
	{order = "revive", sequence = 1, filter = {"SynchronyRacing_sharedSuffering", "health"}},
function (ev)
	if not ev.survived and not damage.Flag.check(ev.type, damage.Flag.BYPASS_DEATH_TRIGGERS) then
		local prevLastHurtPlayer = lastHurtPlayer
		if not lastHurtPlayer and ev.entity.controllable then
			lastHurtPlayer = ev.entity.controllable.playerID
		end

		ev.entity.SynchronyRacing_sharedSuffering.processing = true
		inflictSharedSuffering(findHealthiestEntity(), ev.entity, (ev.remainingDamage or ev.damage or 0) + 1, ev)
		ev.survived = ev.entity.health.health > 0
		ev.entity.SynchronyRacing_sharedSuffering.processing = false

		lastHurtPlayer = prevLastHurtPlayer
	end
end)

event.entitySchemaLoadPlayer.add("addSharedSufferingToPlayers", {order = "overrides", sequence = 2}, function (ev)
	if enableSharedDamage then
		ev.entity.SynchronyRacing_sharedSuffering = {}
	end
end)
