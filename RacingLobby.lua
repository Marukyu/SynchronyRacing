local racingFactions = require "SynchronyRacing.RacingFactions"
local racingProtocol = require "SynchronyRacing.RacingProtocol"
local racingSpectator = require "SynchronyRacing.RacingSpectator"
local controls = require "necro.config.Controls"
local currentLevel = require "necro.game.level.CurrentLevel"

local event = require "necro.event.Event"
local gameState = require "necro.client.GameState"
local lobby = require "necro.client.Lobby"
local multiInstance = require "necro.client.MultiInstance"
local playerList = require "necro.client.PlayerList"
local render = require "necro.render.Render"
local settings = require "necro.config.Settings"
local theme = require "necro.config.Theme"
local ui = require "necro.render.UI"

local color = require "system.utils.Color"
local gfx = require "system.gfx.GFX"
local utils = require "system.utils.Utilities"

useCustomLobbyLevel = settings.shared.bool {
	name = "Use minimal lobby level",
	default = false,
	order = 800,
}

lobbyIntroText = settings.shared.string {
	name = "Intro text",
	desc = "Header text to show on the character selection screen",
	default = "Get ready!",
	order = 2,
}

lobbyIntroSubtitle = settings.shared.string {
	name = "Intro desc.",
	desc = "Subhead text to show on the character selection screen",
	default = "",
	order = 3,
}

local function getPlayerStatus(playerID)
	if not playerList.isConnected(playerID) then
		return "Disconnected", color.rgb(200, 200, 200)
	elseif playerList.isReady(playerID) then
		return "Ready", color.rgb(128, 255, 255)
	else
		return "Not ready", color.rgb(255, 128, 128)
	end
end

local function getPlayersGroupedByTeams(ownMatchID)
	-- Obtain players across all rooms, then sort them into teams
	local teams = {}
	local maxTeamID = 1
	local maxTeamSize = 0

	for _, playerID in ipairs(playerList.getCrossRoomPlayerList()) do
		if racingProtocol.clientGetPlayerAttribute(playerID, racingProtocol.PlayerAttribute.MATCH) == ownMatchID
			and playerList.getAttribute(playerID, racingProtocol.PlayerAttribute.PARTICIPANT)
		then
			local teamID = racingFactions.getPlayerFaction(playerID)
			if teamID and not racingSpectator.isClientSpectator(playerID) then
				maxTeamID = math.max(maxTeamID, teamID)
				teams[teamID] = teams[teamID] or {}
				table.insert(teams[teamID], playerID)
				maxTeamSize = math.max(maxTeamSize, #teams[teamID])
			end
		end
	end

	-- Align teams in the character selector, with odd-numbered teams on the left and even-numbered teams on the right
	local players = {}
	for teamID = 1, maxTeamID, 2 do
		local teamL = teams[teamID]
		local teamR = teams[teamID + 1]
		for i = 1, maxTeamSize do
			players[#players + 1] = teamL and teamL[i] or false
			players[#players + 1] = teamR and teamR[i] or false
		end
	end

	return players
end

event.lobbyUpdatePlayers.add("addCrossRoomPlayers", {order = "players", sequence = 1}, function (ev)
	local ownMatchID = racingProtocol.clientGetPlayerAttribute(nil, racingProtocol.PlayerAttribute.MATCH)
	if not ownMatchID then
		return
	end

	ev.players = getPlayersGroupedByTeams(ownMatchID)

	for _, playerID in ipairs(ev.players) do
		ev.playerData[playerID] = ev.playerData[playerID] or {}
		local playerData = ev.playerData[playerID]
		local teamID = racingFactions.getPlayerFaction(playerID)
		if teamID then
			local subText, subColor = getPlayerStatus(playerID)
			playerData.statusText = "Team " .. tostring(teamID)
			playerData.statusColor = racingFactions.Colors[teamID]
			playerData.subtitleText = subText
			playerData.subtitleColor = subColor
		else
			playerData.statusText = "Waiting..."
			playerData.statusColor = color.rgb(200, 200, 200)
		end
		playerData.host = false
	end

	ev.headerTextOverride = ""
end)

event.gameStateEnterLobby.add("initializeReadOnlyLobby", {order = "init", sequence = -1}, function (ev)
	ev.passiveMode = true
	ev.forceReady = false
end)

event.gameStateEnterLobby.add("autoSpectate", {order = "menu", sequence = 1}, function (ev)
	if racingSpectator.isClientSpectator() then
		lobby.setSpectating(playerList.getLocalPlayerID(), true)
	end
end)

local function renderOverlayText(text, y)
	ui.drawText {
		font = ui.Font.MEDIUM,
		text = text,
		size = 24,
		x = math.floor(gfx.getWidth() * 0.5),
		y = y,
		maxWidth = gfx.getWidth() * 0.6,
		alignX = 0.5,
		alignY = 1,
		buffer = render.Buffer.UI_LOBBY,
	}
end

event.renderUI.add("renderLobbyOverlay", {order = "lobby", sequence = 1}, function ()
	if lobby.isOpen() then
		if multiInstance.isEmbedded() then
			renderOverlayText("Opponent view", 70)
			local y = 110
			for _, playerID in ipairs(playerList.getPlayerList()) do
				if playerList.getAttribute(playerID, racingProtocol.PlayerAttribute.PARTICIPANT) then
					renderOverlayText(tostring(playerList.getName(playerID)), y)
					y = y + 32
				end
			end
			local buffer = render.getBuffer(render.Buffer.UI_LOBBY)
			local frameColor = color.fade(theme.Color.HIGHLIGHT, 0.2)
			local width, height = gfx.getSize()
			local margin = 4
			buffer.draw { rect = { 0, 0, width, margin }, color = frameColor }
			buffer.draw { rect = { 0, height - margin, width, margin }, color = frameColor }
			buffer.draw { rect = { 0, margin, margin, height - margin * 2 }, color = frameColor }
			buffer.draw { rect = { width - margin, margin, margin, height - margin * 2 }, color = frameColor }
			return
		end

		if not lobby.isSpectating() and not gameState.getCountdown() then
			ui.drawText {
				font = ui.Font.SMALL,
				text = playerList.isReady()
					and string.format("Waiting for other players (press %s to cancel)", controls.getFriendlyMiscKeyBind(controls.Misc.CLOSE))
					or string.format("Press %s to get ready!", controls.getFriendlyMiscKeyBind(controls.Misc.SELECT)),
				size = 12,
				x = math.floor(gfx.getWidth() * 0.5),
				maxWidth = gfx.getWidth() * 0.6,
				y = gfx.getHeight() - 60,
				alignX = 0.5,
				buffer = render.Buffer.UI_LOBBY,
			}
		end
		ui.drawText {
			font = ui.Font.MEDIUM,
			text = lobbyIntroText,
			size = 24,
			x = math.floor(gfx.getWidth() * 0.5),
			y = 70,
			maxWidth = gfx.getWidth() * 0.6,
			alignX = 0.5,
			alignY = 1,
			buffer = render.Buffer.UI_LOBBY,
		}
		ui.drawText {
			font = ui.Font.SMALL,
			uppercase = false,
			text = lobbyIntroSubtitle,
			size = ui.Font.SMALL.size * 2,
			x = math.floor(gfx.getWidth() * 0.5),
			y = 80,
			maxWidth = gfx.getWidth() * 0.6,
			wordWrap = true,
			alignX = 0.5,
			alignY = 0,
			buffer = render.Buffer.UI_LOBBY,
		}
	end
end)

event.levelSequenceUpdate.add("overrideLobbyLevel", "training", function (ev)
	if useCustomLobbyLevel then
		ev.options.fileName = "mods/SynchronyRacing/lobby.xml"
	end
end)

event.levelLoad.add("disableLobbyProcGen", {order = "lobbyLevel", sequence = -1}, function (level)
	if useCustomLobbyLevel and currentLevel.isLobby() then
		level.enableLobbyGeneration = false
	end
end)
