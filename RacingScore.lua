local racingScore = {}

local racingConfig = require "SynchronyRacing.RacingConfig"
local racingMatch = require "SynchronyRacing.RacingMatch"
local racingRooms = require "SynchronyRacing.RacingRooms"
local racingProtocol = require "SynchronyRacing.RacingProtocol"
local racingTimer = require "SynchronyRacing.RacingTimer"
local racingUtils = require "SynchronyRacing.RacingUtils"
local levelSequence = require "necro.game.level.LevelSequence"

local serverClock = require "necro.server.ServerClock"
local serverChat = require "necro.server.ServerChat"
local serverRooms = require "necro.server.ServerRooms"
local serverPlayerList = require "necro.server.ServerPlayerList"

local client = require "necro.client.GameClient"
local event = require "necro.event.Event"
local timer = require "system.utils.Timer"

-- TODO implement multi-round matches and scoring
-- TODO support different/custom scoring systems
-- TODO use actual RTA to decide winner

function racingScore.clientSendFlagPlant(levelNumber)
	local entry = levelSequence.get(levelNumber)
	racingProtocol.clientSendMessage(racingProtocol.MessageType.FLAGPLANT, {
		number = levelNumber,
		depth = entry and entry.depth,
		floor = entry and entry.floor,
	})
end

function racingScore.clientSendWin()
	-- TODO clean up this function
	racingProtocol.clientSendMessage(racingProtocol.MessageType.VICTORY)
end

function racingScore.clientSendForfeit()
	-- TODO clean up this function
	racingProtocol.clientSendMessage(racingProtocol.MessageType.FORFEIT)
end

--- Returns the placement number of the next winning team
--- @param match SynchronyRacingMatch
--- @return integer
function racingScore.getPlacement(match)
	-- TODO track placement/score per-round
	local placement = 1
	for _, team in ipairs(match.teams) do
		if team.score and team.score > 0 then
			placement = placement + 1
		end
	end
	return placement
end

--- @param match SynchronyRacingMatch
--- @return integer
function racingScore.getRemainingUndecidedTeams(match)
	local teamCount = 0
	for _, team in ipairs(match.teams) do
		if not team.score then
			teamCount = teamCount + 1
		end
	end
	return teamCount
end

--- Returns whether this match is decisively finished and can be ended
--- @param match SynchronyRacingMatch
--- @return boolean
function racingScore.isMatchDecided(match)
	if racingConfig.getValueForMatch(match, racingConfig.Setting.AUTO_CLOSE_MODE) == racingConfig.AutoCloseMode.ALL then
		for _, team in ipairs(match.teams) do
			if not team.score then
				return false
			end
		end
		return true
	else
		local count = 0
		local anyTeamForfeited = false
		for _, team in ipairs(match.teams) do
			if not team.score then
				count = count + 1
			elseif team.forfeit then
				anyTeamForfeited = true
			end
			if count > 1 then
				return false
			end
		end
		-- If any team forfeited, all players must finish/forfeit
		return not anyTeamForfeited or count == 0
	end
	
end

--- @param match SynchronyRacingMatch
local function getVictoryTimeTable(match)
	local victoryTimes = {}
	for teamID, team in ipairs(match.teams) do
		victoryTimes[teamID] = team.victoryTime
	end
	return victoryTimes
end

--- @param match SynchronyRacingMatch
--- @return number|nil
function racingScore.getMatchSoftCloseTime(match)
    --- @type SynchronyRacingTeam|nil
	if racingConfig.getValueForMatch(match, racingConfig.Setting.AUTO_CLOSE) == true then
		local unfinishedTeam
		for _, team in ipairs(match.teams) do
			if not team.score then
				if unfinishedTeam then
					-- Two or more teams have not yet finished: don't auto-close
					return nil
				else
					unfinishedTeam = team
				end
			end
		end
		if unfinishedTeam then
			-- Exactly one team has not yet finished, auto-close depending on level (30s on story boss, 5 elsewhere)
			return (tonumber(unfinishedTeam.currentLevel and unfinishedTeam.currentLevel.number) or 0) >= racingConfig.getValueForMatch(match, racingConfig.Setting.AUTO_CLOSE_CUTOFF)
				and 30
				or 5
		else
			-- All teams have finished, auto-close after 1 second
			return 1
		end
	else
		return 10
	end
end

--- Completes this team's run, assigning them their placement and score within the match
--- @param match SynchronyRacingMatch
--- @param teamID integer
function racingScore.completeRun(match, teamID)
	local team = match.teams[teamID]
	if team and team.score == nil then
		local placement = racingScore.getPlacement(match)
		team.placement = placement
		team.score = #match.teams - placement + 1

		-- Victory time
		team.victoryTime = racingTimer.serverGetRTATime(team.room)

		if racingConfig.getValueForMatch(match, racingConfig.Setting.VICTORY_MODE) == racingConfig.VictoryMode.TIME then
			local message = string.format("%s finished in %s place!", racingMatch.getTeamLabel(team),
				racingUtils.placementToString(placement))
			local victoryTimes = getVictoryTimeTable(match)
			for _, targetTeam in ipairs(match.teams) do
				serverChat.sendSystemMessage(message, serverRooms.playersInRoom(targetTeam.room))
				racingProtocol.serverSetRoomAttribute(targetTeam.room, racingProtocol.RoomAttribute.VICTORY_TIMES, victoryTimes)
			end
		end

		-- TODO replace automatic soft-close with a better system later
		if racingScore.isMatchDecided(match) and not match.softCloseTimestamp then
			match.softCloseTimestamp = serverClock.getTime() + racingScore.getMatchSoftCloseTime(match)
		end
	end
end

--- Forteits this team's run, giving them 0 points for this run
--- @param match SynchronyRacingMatch
--- @param teamID integer
function racingScore.forfeitRun(match, teamID)
	local team = match.teams[teamID]
	if team and team.score == nil then
		team.forfeit = true
		team.score = 0

		local message = string.format("%s resigned", racingMatch.getTeamLabel(team))
		for _, targetTeam in ipairs(match.teams) do
			serverChat.sendSystemMessage(message, serverRooms.playersInRoom(targetTeam.room))
		end

		-- TODO replace automatic soft-close with a better system later
		if racingScore.isMatchDecided(match) and not match.softCloseTimestamp then
			match.softCloseTimestamp = serverClock.getTime() + racingScore.getMatchSoftCloseTime(match)
		end
	end
end

--- Forteits this team's run, giving them 0 points for this run
--- @param match SynchronyRacingMatch
--- @param teamID integer
--- @param message SynchronyRacingFlagPlant
local function flagPlant(match, teamID, message)
    local team = match.teams[teamID]
    if team and type(message) == "table" then
        local number = tonumber(message.number) or 0
        team.currentLevel = {
            number = number,
            depth = tonumber(message.depth) or 0,
            floor = tonumber(message.floor) or 0,
            time = racingTimer.serverGetRTATime(team.room),
        }
        if number > (tonumber(team.flagPlant and team.flagPlant.number) or -1) then
            team.flagPlant = team.currentLevel
        end
    end
end

local function handleResultMessage(ev, func)
	local room = racingRooms.getRoom(serverPlayerList.getRoomID(ev.playerID))
	if not room then
		return
	end

	local match = racingRooms.getMatch(room.matchID)
	if not match or match.status ~= racingMatch.Status.ACTIVE then
		return
	end

	if not racingRooms.isParticipant(ev.playerID) then
		return
	end

	func(match, room.teamID, ev.message)
end

event.serverMessage.add("racingVictory", racingProtocol.MessageType.VICTORY, function (ev)
	-- TODO config option to only allow spectating referee clients to declare victory
	-- TODO possibly correct for latency/level gen/level transitions?
	handleResultMessage(ev, racingScore.completeRun)
end)

event.serverMessage.add("racingForfeit", racingProtocol.MessageType.FORFEIT, function (ev)
	handleResultMessage(ev, racingScore.forfeitRun)
end)

event.serverMessage.add("racingFlagplant", racingProtocol.MessageType.FLAGPLANT, function (ev)
	handleResultMessage(ev, flagPlant)
end)

return racingScore
