local racingQueue = {}

local event = require "necro.event.Event"

pendingMatches = {}
paused = false

function racingQueue.getPendingMatch(index)
	return pendingMatches[index]
end

function racingQueue.getPendingMatchCount()
	return #pendingMatches
end

function racingQueue.hasPendingMatch()
	return not paused and #pendingMatches ~= 0
end

--- Pops the next match off the queue, if it is available
--- @return SynchronyRacingMatch|nil
function racingQueue.nextMatch()
	if not paused then
		return table.remove(pendingMatches, 1)
	end
end

function racingQueue.pause()
	paused = true
end

function racingQueue.resume()
	paused = false
end

function racingQueue.isPaused()
	return paused
end

function racingQueue.clear()
	pendingMatches = {}
end

function racingQueue.addMatch(match)
	table.insert(pendingMatches, match)
end

function racingQueue.swapMatches(index1, index2)
	if pendingMatches[index1] and pendingMatches[index2] then
		pendingMatches[index1], pendingMatches[index2] = pendingMatches[index2], pendingMatches[index1]
	end
end

event.serverReset.add("resetRacingQueue", "playerList", function (ev)
	pendingMatches = {}
	paused = false
end)

return racingQueue
