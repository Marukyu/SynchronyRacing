local racingOpponentHUD = {}

local racingHologram = require "SynchronyRacing.RacingHologram"
local racingProtocol = require "SynchronyRacing.RacingProtocol"
local racingRooms = require "SynchronyRacing.RacingRooms"
local racingSpectator = require "SynchronyRacing.RacingSpectator"
local racingUtils = require "SynchronyRacing.RacingUtils"

local event = require "necro.event.Event"
local hud = require "necro.render.hud.HUD"
local hudLayout = require "necro.render.hud.HUDLayout"
local currentLevel = require "necro.game.level.CurrentLevel"
local gameSession = require "necro.client.GameSession"
local multiInstance = require "necro.client.MultiInstance"
local music = require "necro.audio.Music"
local playerList = require "necro.client.PlayerList"
local runSummary = require "necro.client.RunSummary"
local settings = require "necro.config.Settings"
local turn = require "necro.cycles.Turn"
local ui = require "necro.render.UI"

local color = require "system.utils.Color"

showOpponentHUD = settings.overridable.bool {
	name = "Show opponent progress HUD",
	default = true,
	order = 100,
}

local cachedHUDText = nil

deathSynced = false

local function levelNum(level)
	return tonumber(type(level) == "table" and level[1] or level)
end

local function getLeadingTeam()
	-- TODO cache this until player attrib changes
	local bestPlayers = {}
	local bestTeam, bestTime, bestLevel
	local victoryTimes = racingProtocol.clientGetRoomAttribute(racingProtocol.RoomAttribute.VICTORY_TIMES) or {}
	local localTeam = racingRooms.getClientPlayerTeamID()

	for _, playerID in ipairs(playerList.getCrossRoomPlayerList()) do
		if not racingSpectator.isClientSpectator(playerID) then
			local teamID = racingRooms.getClientPlayerTeamID(playerID)
			local name = playerList.getName(playerID)
			local time = victoryTimes[teamID]
			local level = racingProtocol.clientGetPlayerAttribute(playerID, racingProtocol.PlayerAttribute.LEVEL)
			if teamID == bestTeam then
				bestPlayers[#bestPlayers + 1] = name
			elseif teamID ~= localTeam and (not bestTeam
				or (time or math.huge) < (bestTime or 0)
				or (bestTime == nil and (levelNum(level) or -math.huge) > (levelNum(bestLevel) or 0)))
			then
				bestTeam = teamID
				bestTime = time
				bestLevel = level
				bestPlayers = {name}
			end
		end
	end
	return bestPlayers, bestTime, bestLevel
end

local function colorize(col)
	local r, g, b = color.getRGBA(col)
	return string.format("\003*%x%x%x", math.floor(r / 16), math.floor(g / 16), math.floor(b / 16))
end

local zoneColors = {
	color.rgb(210, 130, 90),
	color.rgb(40, 170, 40),
	color.rgb(65, 220, 250),
	color.rgb(125, 75, 240),
	color.rgb(180, 180, 40),
}

local function drawProgress(str)
	hud.drawText {
		text = str,
		font = ui.Font.SMALL,
		element = hudLayout.Element.LEVEL,
		alignX = 1,
		alignY = 1,
		maxWidth = 0,
		offsetY = -(ui.Font.SMALL.size + 4),
	}
end

function racingOpponentHUD.formatLevel(depth, level)
	return ui.colorizeText(string.format("%s-%s", depth, level), zoneColors[depth] or color.WHITE)
end

event.renderGlobalHUD.add("renderOpponentProgress", {order = "levelCounter", sequence = 1}, function ()
	if cachedHUDText == nil then
		cachedHUDText = false
		local mode = gameSession.getCurrentMode()
		if mode and mode.levelCounterHUD ~= false and not currentLevel.isLobby()
			and (not multiInstance.isDuplicate() or racingHologram.getElementVisibility("showOpponentProgress"))
		then
			local bestPlayers, bestTime, bestLevel = getLeadingTeam()
			local teamName = table.concat(bestPlayers, ", ", 1, math.min(#bestPlayers, 2))
			if type(bestTime) == "number" then
				cachedHUDText = string.format("%s%s: %s%s", colorize(color.rgb(128, 128, 128)), teamName,
					colorize(color.hsv(0.18, 0.3, 1)), racingUtils.formatTime(bestTime))
			elseif type(bestLevel) == "table" and type(bestLevel[2]) == "number" and type(bestLevel[3]) == "number" then
				if bestLevel[1] == 0 then
					cachedHUDText = string.format("%s%s: %sDEAD!", colorize(color.rgb(128, 128, 128)), teamName,
						colorize(color.rgb(250, 30, 10)))
				else
					local depth, level = bestLevel[2], bestLevel[3]
					cachedHUDText = string.format("%s%s: %s%s-%s", colorize(color.rgb(128, 128, 128)), teamName,
						colorize(zoneColors[depth] or color.WHITE), depth, level)
				end
			end
		end
	end

	if cachedHUDText and showOpponentHUD then
		drawProgress(cachedHUDText)
	end
end)

event.levelLoad.add("racingOpponentHUD", {order = "lobbyLevel", sequence = 1}, function (ev)
	-- TODO set level index by host only
	racingProtocol.clientSetPlayerAttribute(racingProtocol.PlayerAttribute.LEVEL, {
		ev.SynchronyRacing_levelNumberOverride or ev.number or 1,
		ev.SynchronyRacing_levelDepthOverride or ev.depth or 1,
		ev.SynchronyRacing_levelFloorOverride or ev.floor or 1,
	})
	cachedHUDText = nil
	deathSynced = false
end)

event.tick.add("updateRunSummary", "runSummary", function (ev)
	local summary = runSummary.getDecisiveSummary()
	if summary and not deathSynced and not summary.victory and turn.getTurnIDForTime(music.getMusicTime() - 2) > (summary.turnID or 0) then
		racingProtocol.clientSetPlayerAttribute(racingProtocol.PlayerAttribute.LEVEL, {
			0,
			currentLevel.getDepth(),
			currentLevel.getFloor(),
		})
		cachedHUDText = nil
		deathSynced = true
	end
end)

event.clientPlayerList.add("racingOpponentHUD", racingProtocol.PlayerAttribute.LEVEL, function (ev)
	cachedHUDText = nil
end)

event.clientRoomAttribute.add("racingOpponentHUD", racingProtocol.RoomAttribute.VICTORY_TIMES, function (ev)
	cachedHUDText = nil
end)

return racingOpponentHUD
