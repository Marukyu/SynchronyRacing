local racingReset = {}

local racingProtocol = require "SynchronyRacing.RacingProtocol"
local racingRooms = require "SynchronyRacing.RacingRooms"
local racingUsers = require "SynchronyRacing.RacingUsers"

local clientActionBuffer = require "necro.client.ClientActionBuffer"
local currentLevel = require "necro.game.level.CurrentLevel"
local ensemble = require "necro.game.data.modifier.Ensemble"
local event = require "necro.event.Event"
local gameServer = require "necro.server.GameServer"
local gameSession = require "necro.client.GameSession"
local playerList = require "necro.client.PlayerList"
local runSummary = require "necro.client.RunSummary"
local serverActionBuffer = require "necro.server.ServerActionBuffer"
local serverPlayerList = require "necro.server.ServerPlayerList"
local serverRooms = require "necro.server.ServerRooms"
local settings = require "necro.config.Settings"
local settingsStorage = require "necro.config.SettingsStorage"
local singlePlayer = require "necro.client.SinglePlayer"

local enum = require "system.utils.Enum"

allowReset = settings.shared.bool {
	name = "Allow player restart",
	desc = "Allows any racer to quick-restart the run\non the first level or after dying",
	default = true,
	order = 15,
}

racingReset.Target = enum.sequence {
	FIRST_LEVEL = enum.entry(1, {
		func = function ()
			gameSession.restart()
		end,
	}),

	CURRENT_LEVEL = enum.entry(2, {
		func = function ()
			gameSession.goToLevel(currentLevel.getNumber())
		end,
		cancelSequenceSync = true,
	}),

	NEXT_LEVEL = enum.entry(3, {
		func = function ()
			gameSession.nextLevel()
		end,
		cancelSequenceSync = true,
	}),
}

function racingReset.sendRequest(roomID, target)
	if racingRooms.isRacingRoom(roomID) then
		if racingReset.Target.data[target].cancelSequenceSync then
			serverActionBuffer.getActionBuffer(roomID).setSequenceSync(nil)
		end
		racingProtocol.serverSendMessage(serverRooms.playersInRoom(roomID), racingProtocol.MessageType.RESTART, {
			target = target,
		})
	end
end

event.clientRequestRestart.add("skipPermissions", {order = "permissions", sequence = -1}, function (ev)
	if allowReset
		and gameSession.getCurrentMode().allowRestart ~= false
		and settingsStorage.get("gameSession.restart.enabled")
	then
		ev.checkPermissions = false
		ev.showConfirmation = not ev.fromHotkey and not ev.fromConfirmation and not ev.fromRunSummary
	end

	if not gameSession.isRestartAllowed() then
		ev.showConfirmation = false
	end

	-- Disallow quick restart from hotkey in Ensemble char select
	if ensemble.isActive() and currentLevel.isSafe() and not currentLevel.isLobby() then
		ev.suppressed = true
	end

	-- Disable quick restart hotkey in MP unless dead
	if ev.fromHotkey and not singlePlayer.isActive() and currentLevel.getNumber() ~= 1
		and not runSummary.getDecisiveSummary()
	then
		ev.suppressed = true
	end
end)

event.clientRequestRestart.add("performClientRestart", {order = "restart", sequence = -1}, function (ev)
	if not ev.suppressed and (not currentLevel.isLobby() or not gameServer.isOpen()) then
		-- Allow non-hosts to restart by sending a message to the server, which is relayed to the host player
		racingProtocol.clientSendMessage(racingProtocol.MessageType.RESTART)
		ev.success = true
		ev.suppressed = true
	end
end)

event.clientMessage.add("handleRestartMessage", racingProtocol.MessageType.RESTART, function (message)
	if racingReset.Target.data[message.target or racingReset.Target.FIRST_LEVEL].cancelSequenceSync then
		gameSession.cancelPendingTransitions(true)
		clientActionBuffer.releaseSequenceSync()
		clientActionBuffer.getBuffer().setSequenceSync(nil)
		clientActionBuffer.initiateSequenceSync()
	end

	if playerList.isHost() then
		local target = (type(message) == "table" and message.target or racingReset.Target.FIRST_LEVEL)
		local func = racingReset.Target.data[target].func
		if func then
			func()
		end
	end
end)

event.serverMessage.add("forwardRestartMessageToHost", racingProtocol.MessageType.RESTART, function (ev)
	local roomID = serverPlayerList.getRoomID(ev.playerID)
	if racingRooms.isRacingRoom(roomID)
		and (racingUsers.isAdmin(racingUsers.lookUpUserByPlayerID(ev.playerID)) or racingRooms.isParticipant(ev.playerID))
	then
		local hostPlayerID = serverRooms.getHost(roomID)
		if serverPlayerList.isConnected(hostPlayerID) then
			racingProtocol.serverSendMessage(hostPlayerID, racingProtocol.MessageType.RESTART)
		end
	end
end)

return racingReset
