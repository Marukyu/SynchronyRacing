local racingProtocol = require "SynchronyRacing.RacingProtocol"
local racingUsers = require "SynchronyRacing.RacingUsers"
local menu = require "necro.menu.Menu"
local serverPlayerList = require "necro.server.ServerPlayerList"
local serverSocket = require "necro.server.ServerSocket"
local tick = require "necro.cycles.Tick"
local utils = require "system.utils.Utilities"
local racingRPC = {}

local select = select
local unpack = unpack

local registeredFunctions = {}
local clientCache = {}
local clientState = {}
local clientPendingCalls = {}
local clientReceivedCallID = 0
local clientSentCallID = 1
local clientUpdatePending = false
local clientMenuUpdatePending = false
local clientCacheClearPending = false

registeredFunctions, clientCache, clientState, clientPendingCalls, clientReceivedCallID, clientSentCallID = script.persist(function ()
	return registeredFunctions, clientCache, clientState, clientPendingCalls, clientReceivedCallID, clientSentCallID
end)

local function packParameters(...)
	local count = select("#", ...)
	local resolvers = {[0] = count}
	for i = 1, count do
		resolvers[i] = select(i, ...)
	end
	return resolvers
end

local function unpackParameters(list)
	return unpack(list, 1, list[0])
end

local function packResolvers(...)
	local count = select("#", ...)
	if count > 0 then
		local resolvers = {[0] = count}
		for i = 1, count do
			resolvers[i] = select(i, ...)
		end
		return resolvers
	end
end

local callDeferred = tick.registerDelay(function (args)
	if clientReceivedCallID < args.expectedCallID then
		return false
	end
	local rpc = registeredFunctions[args.name]
	if rpc then
		rpc.func(unpackParameters(args.parameters))
	end
end, "RacingRPC")

function racingRPC.clientClearCache()
	utils.clearTable(clientState)
	clientUpdatePending = false
	clientMenuUpdatePending = false
	clientCacheClearPending = false
end

function racingRPC.clientResetAll()
	racingRPC.clientClearCache()
	utils.clearTable(clientCache)
	utils.clearTable(clientPendingCalls)
end

local updateClientDeferred = tick.registerDelay(function ()
	if clientCacheClearPending then
		racingRPC.clientClearCache()
		menu.updateAll()
	elseif clientMenuUpdatePending then
		menu.update()
	end
	clientUpdatePending = false
	clientMenuUpdatePending = false
	clientCacheClearPending = false
end, "RacingRPCUpdate")

local function updateClient(clearCache, updateMenu)
	if not clientUpdatePending then
		clientUpdatePending = true
		updateClientDeferred()
	end
	clientCacheClearPending = clientCacheClearPending or clearCache
	clientMenuUpdatePending = clientMenuUpdatePending or updateMenu
end

local function getOrCreate(tbl, name)
	local cache = tbl[name]
	if not cache then
		cache = {}
		tbl[name] = cache
	end
	return cache
end

local function makeFunctionCall(call)
	if call.defer then
		return function (...)
			local parameters = packParameters(...)
			if call.deferResolver then
				parameters = call.deferResolver(parameters)
			end
			callDeferred {
				name = call.name,
				expectedCallID = clientSentCallID,
				parameters = parameters,
			}
		end
	elseif call.cache then
		return function (...)
			local count = select("#", ...)
			local parameters = { [0] = count }
			local cacheParts = {}
			for i = 1, count do
				local parameter = select(i, ...)
				parameters[i] = parameter
				local resolver = call.resolvers and call.resolvers[i]
				if resolver then
					local success, key = resolver(true, parameter)
					if success then
						cacheParts[i] = tostring(key)
					else
						-- Skip call on resolution failure
						return
					end
				else
					cacheParts[i] = tostring(parameter)
				end
			end
			local cacheKey = table.concat(cacheParts, "\0")
			local state = getOrCreate(clientState, call.name)
			if state[cacheKey] == nil then
				-- Call not yet made: send network message
				clientSentCallID = clientSentCallID + 1
				state[cacheKey] = false
				clientPendingCalls[clientSentCallID] = { key = cacheKey, name = call.name }
				racingProtocol.clientSendMessage(racingProtocol.MessageType.RPC, {
					id = clientSentCallID,
					name = call.name,
					args = parameters,
				})
			end
			return getOrCreate(clientCache, call.name)[cacheKey]
		end
	else
		return function (...)
			clientSentCallID = clientSentCallID + 1
			clientPendingCalls[clientSentCallID] = call.name
			racingProtocol.clientSendMessage(racingProtocol.MessageType.RPC, {
				id = clientSentCallID,
				name = call.name,
				args = packParameters(...),
			})
		end
	end
end

--- Binds a function so that a privileged client can call it on behalf of the server
--- @generic T
--- @param func T
--- @return T
function racingRPC.bind(func, ...)
	return {
		func = func,
		resolvers = packResolvers(...),
		invalidate = true,
		update = true,
	}
end

--- Binds a function so that a privileged client can call it on behalf of the server.
--- @generic T
--- @param setter T
--- @param getter function
--- @return T
function racingRPC.bindSetter(setter, getter, ...)
	return {
		func = setter,
		getter = getter,
		resolvers = packResolvers(...),
		update = true,
	}
end

--- Binds a function so that a privileged client can call it on behalf of the server.
--- Caches the return value for repeated invocations to reduce traffic
--- @generic T
--- @param func T
--- @return T
function racingRPC.bindCache(func, ...)
	return {
		func = func,
		resolvers = packResolvers(...),
		cache = true,
		update = true,
	}
end

--- Binds a function that executes on the client-side once all pending RPC calls so far have completed
--- @generic T
--- @param func T
--- @return T
function racingRPC.bindDeferred(func, resolver)
	return {
		func = func,
		defer = true,
		deferResolver = resolver,
	}
end

--- Registers a table of named functions for remote procedure calls
--- @generic T
--- @param moduleName string
--- @param funcTable T
--- @return T
function racingRPC.register(moduleName, funcTable)
	local result = {}
	local callsByFunction = {}
	local pendingGetters = {}
	for name, call in pairs(funcTable) do
		local fullName = moduleName .. "_" .. name
		call.name = fullName
		if call.func then
			callsByFunction[call.func] = call
		end
		if call.getter then
			pendingGetters[#pendingGetters + 1] = call
		end
		registeredFunctions[fullName] = call
		result[name] = makeFunctionCall(call)
	end

	for _, call in ipairs(pendingGetters) do
		local getterCall = callsByFunction[call.getter]
		if getterCall then
			call.getter = getterCall.name
		else
			call.getter = nil
			log.warn("Unresolved getter for %s", call.name)
		end
	end

	return result
end

function racingRPC.serverInvalidateCaches()
	for _, playerID in ipairs(serverPlayerList.allPlayers()) do
		racingProtocol.serverSendMessage(playerID, racingProtocol.MessageType.RPC, {
			clearCache = true,
		})
	end
end

event.clientMessage.add("racingRPC", racingProtocol.MessageType.RPC, function (message)
	local call
	if type(message.id) == "number" then
		clientReceivedCallID = message.id
		local pending = clientPendingCalls[message.id]
		if type(pending) == "string" then
			call = registeredFunctions[pending]
		elseif type(pending) == "table" then
			getOrCreate(clientState, pending.name)[pending.key] = true
			getOrCreate(clientCache, pending.name)[pending.key] = message.result
			call = registeredFunctions[pending.name]
		end
		clientPendingCalls[message.id] = nil
	elseif message.clearCache then
		return updateClient(true, true)
	end

	if call then
		if call.getter then
			clientState[call.getter] = nil
			clientCache[call.getter] = nil
		end
		updateClient(call.invalidate, call.update)
	end
end)

event.serverMessage.add("racingRPC", racingProtocol.MessageType.RPC, function (ev)
	local userID = racingUsers.lookUpUserByPlayerID(ev.playerID)
	if racingUsers.isAdmin(userID) or serverSocket.isLocalPlayer(ev.playerID) then
		local call = registeredFunctions[ev.message.name]
		if call and call.func then
			if call.resolvers then
				-- Increase number of arguments to match resolvers
				ev.message.args[0] = math.max(ev.message.args[0], call.resolvers[0])
				for i = 1, call.resolvers[0] do
					local resolver = call.resolvers[i]
					if resolver then
						local success, result = resolver(false, ev.message.args[i])
						if success then
							ev.message.args[i] = result
						end
					end
				end
			end
			local success, result = pcall(call.func, unpackParameters(ev.message.args))
			if success then
				racingProtocol.serverSendMessage(ev.playerID, racingProtocol.MessageType.RPC, {
					id = ev.message.id,
					result = result,
				})
			else
				log.error("Call '%s' failed:\n%s", call.name, result)
				racingProtocol.serverSendMessage(ev.playerID, racingProtocol.MessageType.RPC, {
					id = ev.message.id,
					error = result,
				})
			end
		end
	end
end)

event.clientDisconnect.add("clearRPCCache", "reset", function (ev)
	racingRPC.clientResetAll()
end)

return racingRPC
